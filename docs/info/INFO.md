# SATACO in pMSSM Factory
 All information on how, where and why the SATACO package is used in the [pMSSMFactory](https://gitlab.cern.ch/atlas-phys-susy-wg/summaries/pmssm/pMSSMFactory)

## General Workflow of the SATACO Project

![](sataco_workflow.png)

The **blue** boxes are the steps that are the general steps of the pMSSM Factory. The **orange** boxes mark all the addtional steps that are required for the combination algorithm. The **green** and **pink** boxes mark the steps that are required for the learning algorithm.<br>


## Installation

The initialisiation script of the pMSSM factory **init.sh** will source the **pMSSMFactory/setup_sataco.sh** script.<br>
A summary of what they file does is given below.<br>

It is crucial that the **pMSSMFactory/setup_sataco.sh** script is sourced successfully, otherwise the SATACO package and the corresponding scripts will not work.<br>

As explained the installation is done when initializing the factory automatically. It is recommended not to set it up manually, but of course possible.<br>


## Combination of Analyses (with loaded matrix)

In order to work with the combination algorithm, the following steps are needed:<br>

1. Load the global matrix that is stored in the **pMSSMFactory/Analyses/OverlapMatrix/\<ProdType\>**. This is done by executing the following command:

    ```bash
    (bash) $ generateOverlapMatrix.py [-cs/--config_section <config_section>] 
    ```

    This generates the **pMSSMFactory/SATACO/matrices/overlaps/overlap_matrix.txt** file and different statistic matrices that are required for further processing.<br>

    The configs for this process are specified in the **pMSSMFactory/SATACO/configs/sataco.config** file.<br>

2. This part is **optional but recommened**. The application of the CR filter removes allowed combination of SRs that are controlled by the same or overlapping controll regions and make them therefore also overlapping. This is done by executing the following command:

    ```bash
    (bash) $ applyCRFilter.py
    ```

    The overlap matrix is changed accordingly and the new matrix is stored in the **pMSSMFactory/SATACO/matrices/overlaps/overlap_matrix.txt** file.<br>

    The old matrix can be easily restored by rerunning step 1.<br>

3. After this optional step one can run **\<ProdType\>SatacoCombination** in the typical way of the pMSSMFactory for every model point.

## Combination of Analyses (with generated matrix)

If you want to generate a new matrix of shared events from scratch, the following steps are needed:<br>

1. When executing the **\<ProdType\>TruthAcceptance** step you have to add the option **\[-o\]** (For statistical stability, it is recommended to use lots of data). This will generate shared event matrices on the eos.

2. To combine the shared event matrices and copy them your local factory, you have to execute the following command:
    
    ```bash
    (bash) $ copySharedEventsMatrix.py
    ```

    This will copy the shared event matrices from the eos to the **pMSSMFactory/SATACO/matrices/shared_events** directory and timestamp them.<br>
   
3. To generate the overlap matrix from scratch, you have to execute the following command:

    ```bash
    (bash) $ generateOverlapMatrix.py [-cs/--config_section DEFAULT] -o
    ```

    The -o flog is needed to tell the script that a new overlap matrix should be generated. The new matrix is stored in the **pMSSMFactory/SATACO/matrices/overlaps/overlap_matrix.txt** file.<br>

    You can still go with the global matrix by leaving out the -o flag.<br>

4. After this you can follow the steps 2 and 3 from the previous section.

## Learning Algorithm

The learning algorithm is used to actively learn the next points that are requested for the event generation that will populate a certain combination.<br>

To start with the learning algorithm, you have to have generated the overlap matrix either from scratch or loaded.<br>

Before running the algorithm, check that your configuration file is valid.<br>

1. The next two steps are two commands can be executed in parallel but do similar things. One of them is putting all parameters of each model point into a csv file.
    ```bash
    (bash) $ pMSSMParamsToCSV.py
    ```
    The other one is putting all acceptances of each model point into a csv file.
    ```bash
    (bash) $ acceptancesToCSV.py
    ```
    Both files are stored in the **pMSSMFactory/SATACO/pmssm** directory.<br>

2. Afterwards you can run the command to propose combinations that could be learned to improve the overlap matrix.<br>

    ```bash
    (bash) $ proposeLearnableOverlapCombinations.py [--save] 
    ```

    If the save flag is not raised, the proposed combinations are only printed to the command line. If the save flag is raised, the proposed combinations are stored in the **pMSSMFactory/SATACO/matrices/statistics/proposed_combinations.txt** file.<br>

    Proposing combinations means that the algorithm looks at all matrix entries of the sufficient statistics matrix that are equal to -1 and proposes combinations that are learnable. Additionally, it cross checks if the combination is learnable by looking at the csv files that were generated one step before. If both signal regions accepted at least on event, the combination is considered to be learnable.<br>

    You can just copy the values that are printed on the command line and inject them into the learning algorithm later on.<br>

3. The next step is to actively learn the next points that are requested for the event generation that will populate a certain combination.<br>

    ```bash
    (bash) $ activelyLearnOverlaps.py [-cs/--config_section <config_section>] [-comb/--combination '<SR_i&SR_j>'] 
    ```

    This is a batch job and will take some time. The outputs of this are stored in the **pMSSMFactory/SATACO/pmssm/pmssm_next_points.csv** file.<br>


4. The model generation from these csv files is not yet implemented. This is the next step that needs to be done.<br>

## Other useful commands

Listing all the config values for the combination algorithm:

```bash
(bash) $ satacoConfigs.py
```

Listing all the config values for the learning algorithm:

```bash
(bash) $ learningConfigs.py
```

To get a good a visual analysis of the overlap matrix, you can use the following command:

```bash
(bash) $ plotSatcoMatrices.py [-i/--index_i <starting_index> <ending_index>] [-llh/--likelihood_scans] [-nl/--no_labels] [-gp/--group_production_mode]
```

This will generate multiple plots that will help interpreting the matrix.<br>

Visualizing the changes that the CR filter does to the overlap matrix:

```bash
(bash) $ plotCRFilterComparison.py [-cs/--config_section <config_section>] [-llh/--likelihood_scans] [-i/--index_i <starting_index> <ending_index>] [-nl/--no_labels] [-gp/--group_production_mode]
```

The graphs will show the absolute value with which the overlap matrix changes as well as the if entries are changed at all or not.<br>

To plot distributions of single matrix entries when generating the overlap matrix from scratch, you can use the following command:

```bash
(bash) $ plotSatcoMatrixEntries.py -i/--indices_i "<matrix_entry_i>,<matrix_entry_j>,..." -j/--indices_j "<matrix_entry_k>,<matrix_entry_l>,..." [-cs/--config_section <config_section>] [-a/--amount_of_models <amount_of_models>]
```

To get the distribution of pMSSM parameters that do not exceed a certain threshold across all SRs, you can use the following command:

```bash
(bash) $ plotLowEfficiencyDistribution.py [-t/--threshold <threshold>] 
```

When setting the threshold very high (t>=1), you can see the distribution of all parameter model points. This command should indicate a new research direction because it hints at parameters that are not covered by any or only few SR.<br>


Another plotting method is to plot the accepted events per SR for each Analysis in a bar plot. This can be done with the following command:

```bash
(bash) $ plotAcceptedEvents.py [-f/--focus <analysis_name>]
```

By default this method plots the accepted events for all analyses. If you want to focus on a single analysis, you can use the -f flag.<br>

To plot the

To plot the contours of the SATACO combination algorithm, you can use the following command:

```bash
(bash) $ plotSatcoContours.py
```

However, this command is not yet fully implemented.<br>

