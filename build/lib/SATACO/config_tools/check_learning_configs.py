"""
Functions for checking the validity of the arguments for the
learning algorithm.
"""

from typing import List


def __check_num_next_points(num_next_points: int) -> bool:
    """
    Check if the number of next points is an integer.

    Args: 
        num_next_points (int): The number of next points.

    Returns:
        bool: True if the number of next points is an integer, else False.
    """
    # check if type is integer
    if not isinstance(num_next_points, int):
        return False

    # check if number of next points is greater than 0
    if num_next_points <= 0:
        return False

    # return true if passed the checks
    return True


def __check_acquisition_function(acquisition_function: str) -> bool:
    """
    Check if the acquisition function is valid.

    Args:
        acquisition_function (str): The acquisition function. Should be
            either 'EI', 'PI' or 'LCB'.

    Returns:
        bool: True if the acquisition function is valid, else False.
    """
    # check if type is string
    if not isinstance(acquisition_function, str):
        return False

    # check if in valid selection
    if acquisition_function not in ['EI', 'PI', 'LCB', 'GP_HEDGE']:
        return False

    # return true if passed the checks
    return True


def __check_minimization_function(minimization_function: str) -> bool:
    """
    Check if the minimization function is valid.

    Args:
        minimization_function (str): The minimization function. Should be
            either 'DUMMY', 'FOREST', 'GP', 'GBRT'.

    Returns:
        bool: True if the minimization function is valid, else False.
    """
    # check if type is string
    if not isinstance(minimization_function, str):
        return False

    # check if in valid selection
    if minimization_function not in ['DUMMY', 'FOREST', 'GP', 'GBRT']:
        return False

    # return true if passed the checks
    return True


def __check_acquisition_optimizer(minimization_function: str) -> bool:
    """
    Check if the acquisition optimizer is valid.

    Args:
        minimization_function (str): The acquisition optimizer should be
            either 'SAMPLING', 'LBFGS', 'AUTO'.

    Returns:
        bool: True if the minimization function is valid, else False.
    """
    # check if type is string
    if not isinstance(minimization_function, str):
        return False

    # check if in valid selection
    if minimization_function not in ['SAMPLING', 'LBFGS', 'AUTO']:
        return False

    # return true if passed the checks
    return True


def __check_pmssm_parameters(pmssm_paramters: str) -> bool:
    """
    Check if the pmssm paramters are valid.

    Args:
        pmssm_paramters (str): Different pmssm parameters in a
            comma seperateed form.

    Returns:
        bool: True if the parameters are valid, else False.
    """
    # valid pmssm parameters
    # 'tanb',
    # 'M_1',
    # 'M_2',
    # 'M_3',
    # 'At',
    # 'Ab',
    # 'Atau',
    # 'mu',
    # 'mA',
    # 'meL',
    # 'mmuL',
    # 'mtauL',
    # 'meR',
    # 'mmuR',
    # 'mtauR',
    # 'mqL1',
    # 'mqL2',
    # 'mqL3',
    # 'muR',
    # 'mcR',
    # 'mtR',
    # 'mdR',
    # 'msR',
    # 'mbR'

    valid_parameters: List[str] = [
        'tanb',
        'M_1',
        'M_2',
        'M_3',
        'At',
        'Ab',
        'Atau',
        'mu',
        'mA',
        'meL',
        'mmuL',
        'mtauL',
        'meR',
        'mmuR',
        'mtauR',
        'mqL1',
        'mqL2',
        'mqL3',
        'muR',
        'mcR',
        'mtR',
        'mdR',
        'msR',
        'mbR'
    ]

    # check if type is string
    if not isinstance(pmssm_paramters, str):
        return False

    # if argument is ALL
    if pmssm_paramters == "ALL":
        return True

    # split the string
    pmssm_paramters: List[str] = pmssm_paramters.split(",")

    # remove whitespaces
    pmssm_paramters = [pmssm_param.strip() for pmssm_param in pmssm_paramters]

    # check for all parameters in the list that they are valid options
    for pmssm_param in pmssm_paramters:
        if pmssm_param.strip() not in valid_parameters:
            return False

    # check if there are duplicates
    if len(pmssm_paramters) != len(list(set(pmssm_paramters))):
        return False

    # check if the length is not zero
    if len(pmssm_paramters) == 0:
        return False

    else:
        return True


def __check_signal_eff_function(signal_eff_function: str) -> bool:
    """
    Check if the signal efficiency function is valid.

    Args:
        signal_eff_function (str): The signal efficiency function. Should be
            either 'ONE' or 'TWO'.

    Returns:
        bool: True if the signal efficiency function is valid, else False.
    """
    # check if type is string
    if not isinstance(signal_eff_function, str):
        return False

    # check if in valid selection
    if signal_eff_function not in ['ONE', 'TWO']:
        return False

    # return true if passed the checks
    return True


def __check_signal_eff_alpha(signal_eff_alpha: float) -> bool:
    """
    Check if the signal efficiency alpha is valid.

    Args:
        signal_eff_alpha (float): The signal efficiency alpha. Should be
            between 0 and 1.

    Returns:
        bool: True if the signal efficiency alpha is valid, else False.
    """
    # check if type is float
    if not isinstance(signal_eff_alpha, float):
        return False

    # check if in valid selection
    if signal_eff_alpha <= 0 or signal_eff_alpha >= 1:
        return False

    # return true if passed the checks
    return True


def __check_learning_configs(config_values: dict) -> bool:
    """
    Check if the config file arguments are valid.

    Args:
        config_values (dict): A dictionary containing the values from
            the configuration file.

    Returns:
        bool: True if the config file arguments are valid, else False.
    """
    # check if acquisition function is valid
    if not __check_acquisition_function(
            config_values['acquisition_function']):
        return False

    # check if minimization function is valid
    if not __check_minimization_function(
            config_values['minimization_function']):
        return False

    # check if acquisition optimizer is valid
    if not __check_acquisition_optimizer(
            config_values['acquisition_optimizer']):
        return False

    # check if pmssm parameters are valid
    if not __check_pmssm_parameters(
            config_values['pmssm_parameters']):
        return False

    # check if signal efficiency function is valid
    if not __check_signal_eff_function(
            config_values['signal_eff_function']):
        return False

    # check if signal efficiency alpha is valid
    if not __check_signal_eff_alpha(
            config_values['signal_eff_alpha']):
        return False

    # return true if passed the checks
    return True
