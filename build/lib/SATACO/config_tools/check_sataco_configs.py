"""
The functions  in this script check the type
and the value of the config file arguments.
"""

from typing import Dict


def __check_sataco_configs(config_values: Dict) -> bool:
    """
    Check the type and value of the config file arguments.

    Args:
        config_values: A dictionary containing the values from the
                       configuration file.

    Return
        bool: If the arguments are of correct type and value or not.
    """

    # check if the weight method argument is of correct type
    if not __check_weight_method(config_values['weight_method']):
        return False

    # check if the correlation method argument is of correct type
    if not __check_correlation_method(config_values['correlation_method']):
        return False

    # check if the confidence level argument is of correct type
    if not __check_confidence_level(config_values['confidence_level']):
        return False

    # check if the correlation threshold argument is of correct type
    if not __check_correlation_threshold(
            config_values['correlation_threshold']):
        return False

    # check if the combination method argument is of correct type
    if not __check_combination_method(config_values['combination_method']):
        return False

    # check if the top combinations argument is of correct type
    if not __check_top_combinations(config_values['top_combinations']):
        return False

    # return true if passed the checks
    return True


def __check_weight_method(weight_method) -> bool:
    """
    Check type of the weight method argument
    from the config file.

    Args:
        weight_method: Should be of type string and
                       'unit',
                       'gaussian_approx_wo_error',
                       'gaussian_approx_w_error',
                       'recommended_sensitivity_estimation',
                       'recommended_sensitivity_estimation_wo_error',

    Return
        bool: If the argument is of corrrect type or not.
    """

    # check if type is string
    if not isinstance(weight_method, str):
        return False

    # check if in valid selection
    if weight_method not in [
        'unit',
        'gaussian_approx_wo_error',
        'gaussian_approx_w_error',
        'recommended_sensitivity_estimation',
        'recommended_sensitivity_estimation_wo_error'
    ]:
        return False

    # return true if passed the checks
    return True


def __check_correlation_method(correlation_method) -> bool:
    """
    Check type of the correlation method argument
    from the config file.

    Args:
        correlation_method: Should be of type string and
            and 'pearson', 'geometrical', 'jaccard', 
            'sorensen_dice', 'szymkiewicz_simpson',
            'otsuka_ochiai', 'cosine'.
    Return
        bool: If the argument is of correct type or not.
    """

    # check if type is string
    if not isinstance(correlation_method, str):
        return False

    # check if in valid selection
    if str(correlation_method) not in ['pearson',
                                       'geometrical',
                                       'jaccard',
                                       'sorensen_dice',
                                       'szymkiewicz_simpson',
                                       'otsuka_ochiai',
                                       'cosine']:
        return False

    # return true if passed the checks
    return True


def __check_confidence_level(confidence_level) -> bool:
    """
    Check type of the confidence level argument
    from the config file.

    Args:
        confidence_level: Should be of type float and
                          be between 0 and 1.
    Return
        bool: If the argument is of correct type or not.
    """

    # check if type is float
    if not isinstance(confidence_level, float):
        return False

    # check if between 0 and 1
    if confidence_level <= 0 or confidence_level >= 1:
        return False

    # return true if passed the checks
    return True


def __check_correlation_threshold(correlation_threshold) -> bool:
    """
    Check type of the correlation threshold argument
    from the config file.

    Args:
        correlation_threshold: Should be of type float and
                               be between 0 and 1.
    Return
        bool: If the argument is of correct type or not.
    """

    # check if type is float
    if not isinstance(correlation_threshold, float):
        return False

    # check if between 0 and 1
    if correlation_threshold <= 0 or correlation_threshold >= 1:
        return False

    # return true if passed the checks
    return True


def __check_combination_method(combination_method) -> bool:
    """
    Check type of the combination method argument
    from the config file.

    Args:
        combination_method: Should be of type string and
                            be either 'stouffer' or 'adding'.
    Return
        bool: If the argument is of correct type or not.
    """

    # check if type is string
    if not isinstance(combination_method, str):
        return False

    # check if in valid selection
    if combination_method not in ['stouffer', 'adding']:
        return False

    # return true if passed the checks
    return True


def __check_top_combinations(top_combinations) -> bool:
    """
    Check type of the top combinations argument
    from the config file.

    Args:
        top_combinations: Should be of type int and
                          be greater than 0.
    Return
        bool: If the argument is of correct type or not.
    """

    # check if type is int
    if not isinstance(top_combinations, int):
        return False

    # check if greater than 0
    if top_combinations <= 0:
        return False

    # return true if passed the checks
    return True
