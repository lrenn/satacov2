"""
Propose combinations from statistics matrices.
"""

from typing import Dict, List

import numpy as np


def propose_combinations(significant_correlations: np.ndarray,
                         column_names: List[str]) -> Dict[str, str]:
    """
    Propose combinations from statistics matrices for the active learning
    process.

    Args:
        significant_correlations (np.ndarray): Significant correlations matrix.
        column_names (List[str]): List of column names.

    Returns:
        Dict[str, str]: Dictionary with the proposed combinations
            with the names in the 
            format "column_names[i]&column_names[j]: 'i&j'".
    """
    # Initialize the dictionary
    proposed_combinations: Dict[str, str] = {}

    # For sanity check, len of columns should be equal to the
    # shape[0] of the matrix
    if len(column_names) != significant_correlations.shape[0]:
        print("Column names and the matrix are different "
              "in size which is forbidden.")
        raise ValueError

    # Get all indices where the entries are -1 (not enough data) only search
    # in the lower triangle
    not_enough_data_indices: np.ndarray = np.where(
        np.tril(significant_correlations) == -1)

    # Put the indices in the dictionary in the format
    # "column_names[i]&column_names[j]: 'i&j'"
    for i in range(len(not_enough_data_indices[0])):
        proposed_combinations[
            column_names[not_enough_data_indices[0][i]] +
            '&' + column_names[not_enough_data_indices[1][i]]] = str(
            not_enough_data_indices[0][i]) + '&' + str(
            not_enough_data_indices[1][i])

    # return the dictionary
    return proposed_combinations
