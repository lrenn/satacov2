"""Sort matrices, vectors and list by weight."""
from typing import Any, Dict, List, Tuple

import numpy as np


def sort_correlation_matrix(yield_dict: Dict[str, Dict[str, Any]],
                            correlation_matrix: np.ndarray,
                            combination_names: List[str],
                            combination: str = 'SR',
                            sort: bool = True
                            ) -> Tuple[np.ndarray,
                                       List[str],
                                       List[int]]:
    """
    Sort the correlation matrix with decreasing sensitivity.

    Args:
        yield_dict (Dict[str,Dict[str, Any]]): Dictionary with the depending yields.
        correlation_matrix (np.ndarray): Correlation matrix.
        combination_names (List[str]): List of combination names (SR, Analysis) read from the
            correlation matrix.
        combination (str, optional): Combination name. Defaults to 'SR'.
            Also possible is 'Analysis'.
        sort (bool, optional): Sort the correlation matrix. Defaults to True.

    Returns:
        Tuple[np.ndarray, List[str], List[int]]: Sorted correlation matrix, sorted combination names
            and sorted combination indices.

    """
    if sort:
        # check if the yield dictionary is combined or not ('model_numbers' key
        # must be list).
        if not 'model_numbers' in yield_dict.keys():
            raise ValueError("The yield dictionary is not combined.\n"
                             "First combine the different models with certain function.")
        if not isinstance(yield_dict['model_numbers'], list):
            raise TypeError("The yield dictionary is not combined.\n"
                            "First combine the different models with certain function.")

        # only when the yield dictionary is combined, all signal regions only have one sensitivity
        # value on which the sorting is based

        # sort the dataframe within the dictionary by the sensitivity value, but first check if
        # the sensitivity value is in the dataframe
        if not 'sensitivity' in yield_dict['df_yields'].columns:
            raise ValueError("The sensitivity value is not in the dataframe.\n"
                             "First calculate the sensitivity value for each signal region.")

        # sort the dataframe by the sensitivity value
        yield_dict['df_yields'] = yield_dict['df_yields'].sort_values(
            by='sensitivity', ascending=False)

        # create a list of the signal region names sorted by the sensitivity value
        sorted_combination_names_list: List[str] = list(
            yield_dict['df_yields'][combination])

        # from the sorted combinations and the combination names create an index list
        # which is used to sort the correlation matrix
        sorted_combination_indices: List[int] = []

        # sorted columns names
        sorted_columns_names: List[str] = []
        # iterate over the sorted combination names and get the index of the combination
        # name in the combination names list
        for combination_name in sorted_combination_names_list:
            if combination_name in combination_names:
                sorted_combination_indices.append(
                    combination_names.index(combination_name))
                sorted_columns_names.append(combination_name)

        # sort the correlation matrix by the sorted combination indices
        sorted_correlation_matrix: np.ndarray = correlation_matrix[sorted_combination_indices, :]
        sorted_correlation_matrix = sorted_correlation_matrix[:,
                                                              sorted_combination_indices]

        # return the sorted correlation matrix
        return sorted_correlation_matrix, sorted_columns_names, sorted_combination_indices
    else:
        # return the unsorted correlation matrix
        return correlation_matrix, combination_names, list(range(len(combination_names)))
