"""
Load global correlation matrix from data.
"""

import numpy as np
import os
from typing import Tuple, List


def load_global_correlation_matrix(combination: str = 'SR',
                                   correlation_matrix: str = 'global',
                                   global_correlation_matrix_folder: str = os.path.join(os.path.dirname(__file__),
                                                                                        "..",
                                                                                        "..",
                                                                                        "data",
                                                                                        "correlations")) -> Tuple[np.ndarray, List[str]]:
    """
    Load global correlation matrix from data.

    Args:
        combination (str): The combination of the signal regions. Default is 'SR'.
                           Other option is 'Analysis'."
        correlation_matrix (str): If 'global', the global correlation matrix is loaded.
                                  If 'local', the correlation matrix is not loaded but
                                  calculated on the fly. Default is 'global'.
        global_correlation_matrix_folder (str): The folder where the global correlation matrix is stored.

    Returns:
        Tuple[np.ndarray, List[str]]: The global correlation matrix and the headers (sginal region/analysis names).

    """
    # name of signal regions or analyses dependent on the combination
    headers: List[str] = []
    # check if combination is valid
    if combination not in ['SR', 'Analysis']:
        raise ValueError(
            f"Invalid combination {combination}. Valid options are 'SR' and 'Analysis'.")

    # check if correlation_matrix is valid
    if correlation_matrix not in ['global', 'local']:
        raise ValueError(
            f"Invalid correlation_matrix {correlation_matrix}. Valid options are 'global' and 'local'.")

    # if correlation_matrix is local, calculate it on the fly
    if correlation_matrix == 'local':
        global_correlation_matrix: np.ndarray = None
    # if global, load it from file
    else:
        # file name
        file_name: str = f"{combination}_{combination}.txt"

        # path to file
        global_correlation_matrix_path: str = os.path.join(global_correlation_matrix_folder,
                                                           file_name)

        # check if file exists
        if not os.path.exists(global_correlation_matrix_path):
            raise FileNotFoundError(
                f"The file {global_correlation_matrix_path} does not exist.")

        # load file
        global_correlation_matrix: np.ndarray = np.loadtxt(global_correlation_matrix_path,
                                                           delimiter=',',
                                                           skiprows=1)

        # just read header of file
        with open(global_correlation_matrix_path, 'r') as file:
            headers = [header.strip() for header in file.readline().split(',')]

    return global_correlation_matrix, headers
