"""
Transforms the output of the TACO algorithm to a more
readable format.
"""

from typing import Dict, List


def __indices_to_sr_names(sr_names: List[str],
                          result_dict: Dict) -> Dict:
    """Maps the indices of the Signal Regions to their
    corresponding names.

    Args:
        sr_names (List[str]): List of names of the Signal Regions.
        result_dict (Dict): Dictionary with indices of the Signal
            Regions.

    Returns:
        Dict: Dictionary with SR names instead
        of indices.
    """
    # 1. Define path list.
    path_list: List = []
    # 2. Loop over all paths in dictionary.
    for path_idx, path in result_dict.items():
        path_list = path["path"]
        # 1. Loop over all indices in path.
        for idx, SR_idx in enumerate(path_list):
            # 1. Map index to SR name.
            path_list[idx] = sr_names[SR_idx]
        result_dict[path_idx]["path"] = path_list
    return result_dict
