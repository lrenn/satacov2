"""
Graph class used for the path finder
algorithm.
"""
from typing import Dict, List


class Graph(object):
    def __init__(self):
        # Initialize the adjacency and node dictionaries
        self._adj: Dict = dict()
        self._node: Dict = dict()

    def __construct_nodes(self,
                          edge: List) -> None:
        """
        Construct the node dictionary based on the edges.

        Args:
            edge (List): A list of edges in the form of tuples
                (source, child, weight).

        Returns:
            None.
        """
        # Construct the node dictionary based on the edges
        for item in edge:
            if item[0] not in self._node:
                # If the node is not already in the dictionary, add it
                self._node[item[0]] = {}
                # Also add an empty dictionary for the node's adjacency list
                self._adj[item[0]] = {}

    def __construct_adj(self,
                        edge: List) -> None:
        """
        Construct the adjacency dictionary based on the edges.

        Args:
            edge (List): A list of edges in the form of tuples
            (source, child, weight).

        Returns:
            None.
        """
        # Construct the adjacency dictionary based on the edges
        self.__construct_nodes(edge)
        for item in edge:
            source, child, weight = item
            if child not in self._adj[source]:
                # If the child is not already in the source node's adjacency
                # list, add it
                self._adj[source][child] = {}
            # Add the edge weight to the adjacency list
            index = len(self._adj[source][child])
            self._adj[source][child] = {index: {'weight': weight}}

    def add_weighted_edges(self,
                           edges: List) -> None:
        """
        Add weighted edges to the graph.

        Args:
            edges (List): A list of edges in the form of tuples
                (source, child, weight).

        Returns:
            None.
        """
        # Add weighted edges to the graph
        self.__construct_adj(edges)

    def edges(self,
              srce: int = None) -> List:
        """
        Return a list of edges in the graph.

        Args:
            srce (int): The source node to return edges from. If None,
                return all edges in the graph.

        Returns:
            List: A list of edges in the form of tuples (source, child).
        """
        if isinstance(srce, list):
            # If srce is a list, use the first element as the source node
            srce = srce[0]
        if srce is None:
            # If srce is None, return all edges in the graph
            return [(k, i) for k, subdict in self._adj.items()
                    for i in subdict]
        if srce in self._adj:
            # If srce is a valid source node, return all edges that
            # originate from it
            return [(srce, i) for i in self._adj[srce]]
        else:
            # If srce is not a valid source node, return an empty list
            return []
