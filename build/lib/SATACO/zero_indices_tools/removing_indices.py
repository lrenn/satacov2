"""
Remove indices from correlation matrix that have zeros sensitivity.
"""

from typing import Dict, Any, List, Tuple

import numpy as np


def remove_zero_sensitivities_combinations(correlation_matrix: np.ndarray,
                                           combination_names: List[str],
                                           yield_dict: Dict[str, Dict[str, Any]],
                                           combination: str = 'SR',) -> Tuple[np.ndarray, List[str]]:
    """
    Remove zero sensitivities from correlation matrix.

    Args:
        correlation_matrix (np.ndarray): Correlation matrix.
        combination_names (List[str]): List of analysis and SR names.
        yield_dict (Dict[str, Dict[str, Any]]): Dictionary with yield information.
        combination (str, optional): Combination name. Defaults to 'SR'.

    Returns:
        Tuple[np.ndarray, List[str]]: Correlation matrix without zero sensitivities and
            list of combination names without zero sensitivities.
    """
    # check if the yield dictionary is combined or not ('model_numbers' key
    # must be list).
    if not 'model_numbers' in yield_dict.keys():
        raise ValueError("The yield dictionary is not combined.\n"
                         "First combine the different models with certain function.")
    if not isinstance(yield_dict['model_numbers'], list):
        raise TypeError("The yield dictionary is not combined.\n"
                        "First combine the different models with certain function.")

    # check if the dataframe has the key "sensitivity"
    if not 'sensitivity' in yield_dict['df_yields'].columns:
        raise ValueError("The sensitivity value is not in the dataframe.\n"
                         "First calculate the sensitivity value for each signal region.")

    # get the indices of the zero sensitivities
    zero_indices = np.where(yield_dict["df_yields"]["sensitivity"] == 0)[0]

    # get the combination name from the dataframe with the zero sensitivities
    zero_combination_names: List[str] = list(
        yield_dict["df_yields"].loc[zero_indices][combination])

    # get the indices of the zero sensitivities in the combination names list
    zero_combination_indices: List[int] = []
    for zero_combination_name in zero_combination_names:
        zero_combination_indices.append(
            combination_names.index(zero_combination_name))

    # remove the zero sensitivities from the correlation matrix
    correlation_matrix = np.delete(
        correlation_matrix,
        zero_combination_indices,
        axis=0)
    correlation_matrix = np.delete(
        correlation_matrix,
        zero_combination_indices,
        axis=1)

    # remove the zero sensitivities from the combination names list
    for zero_combination_name in zero_combination_names:
        combination_names.remove(zero_combination_name)

    return correlation_matrix, combination_names
