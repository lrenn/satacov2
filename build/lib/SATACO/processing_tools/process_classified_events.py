"""Processing of root files.
"""

import os
import sys
from typing import Dict, List, Any, Callable, Tuple
import re

import numpy as np
import uproot as ur
import pandas as pd

from SATACO.processing_tools.process_overlaps import pearson_correlation
from SATACO.processing_tools.process_overlaps import geometrical_index
from SATACO.processing_tools.process_overlaps import jaccard_index
from SATACO.processing_tools.process_overlaps import cosine_index
from SATACO.processing_tools.process_overlaps import otsuka_ochiai_index
from SATACO.processing_tools.process_overlaps import sorensen_dice_index
from SATACO.processing_tools.process_overlaps import szymkiewicz_simpson_index


# chunksize for splitting matrices
CHUNK: int = 100


def __validate_model_input(cmd_arg: str) -> bool:
    """
    This function receives an argument from the command line
    which contains specification about the models that should be analyzed
    and returns True if the input is valid and False if the input is invalid.

    Args:
        cmd_arg (str): Argument from command line which contains
                          specification about the models that should be analyzed.

    Returns:
        bool: True if the input is valid and False if the input is invalid.
    """
    pattern = r'^(\s*\d+\s*(?:-\s*\d+\s*)?(?:,\s*\d+\s*(?:-\s*\d+\s*)?)*)+$'
    if re.match(pattern, cmd_arg):
        return True
    else:
        return False


def __store_model_numbers(cmd_arg: str) -> List[int]:
    """
    This function receives an argument from the command line
    which contains specification about the models that should be analyzed
    and returns a list of model numbers.

    Args:
        cmd_arg (str): Argument from command line which contains
                          specification about the models that should be analyzed.

    Returns:
        List[int]: List of model numbers.
    """
    result = []

    parts = cmd_arg.split(',')
    for part in parts:
        part = part.strip()
        if '-' in part:
            range_parts = part.split('-')
            if len(range_parts) != 2:
                continue
            start, end = map(int, range_parts)
            result.extend(range(start, end + 1))
        elif part.isdigit() and part != "0":
            result.append(int(part))

    # sort the result and remove duplicates
    result = sorted(list(set(result)))

    return result


def get_path_classified_event_files(cmd_arg: str,
                                    path_to_directory_of_result_files: str) -> Dict[str, List[str]]:
    """
    This function receives an argument from the command line
    which contains specification about the models that should be analyzed
    and return a list of paths to the corresponding classified event and
    yield files.

    Args:
        cmd_arg (str): Argument from command line which contains
                       specification about the models that should be analyzed.
        path_to_directory_of_result_files (str): Path to directory of result
                                                    files.

    Returns:
        (Dict[str, List[str]]): List of paths to the corresponding classified event and
                   yield files.

    """

    # check if the path to the directory of result files exists
    if not os.path.exists(path_to_directory_of_result_files):
        raise SystemExit(f"Path to directory of result files does not exist: "
                         f"{path_to_directory_of_result_files}")

    # check if the cmd_arg is valid
    if not __validate_model_input(cmd_arg):
        raise SystemExit(f"Invalid input: {cmd_arg}")

    # initialize dictionary
    path_dict: Dict[str, List[str]] = {"yield_paths": [],
                                       "event_paths": []}

    # get the paths to the yield and event files (path to result directory +
    # model name + .root/txt)
    model_numbers = __store_model_numbers(cmd_arg)
    if model_numbers == []:
        # get all possible file paths starting with a number
        # ending with .root and .txt in the directory
        for file in os.listdir(path_to_directory_of_result_files):
            # check if the file starts with a number and ends with .txt
            if file.split(".")[0].isdigit() and file.endswith(".txt"):
                path_dict["yield_paths"].append(os.path.join(path_to_directory_of_result_files,
                                                             file))
            # check if the file starts with a number and ends with .root
            elif file.split(".")[0].isdigit() and file.endswith(".root"):
                path_dict["event_paths"].append(os.path.join(path_to_directory_of_result_files,
                                                             file))
    else:
        for model_number in model_numbers:
            # check if the file exists and afterwards append the path to the
            # dictionary
            if os.path.exists(os.path.join(path_to_directory_of_result_files,
                                           f"{model_number}.txt")):
                path_dict["yield_paths"].append(os.path.join(path_to_directory_of_result_files,
                                                             f"{model_number}.txt"))

            else:
                # TODO: Too many prints
                # print(f"File does not exist: {model_number}.txt")
                continue

            if os.path.exists(os.path.join(path_to_directory_of_result_files,
                                           f"{model_number}.root")):
                path_dict["event_paths"].append(os.path.join(path_to_directory_of_result_files,
                                                             f"{model_number}.root"))

            else:
                # TODO: Too many prints
                # print(f"File does not exist: {model_number}.root")
                continue

    # sort the paths (this is just necessary for 0 cmd_arg input otherwise the
    # sorting is already done)
    path_dict["yield_paths"] = sorted(path_dict["yield_paths"])
    path_dict["event_paths"] = sorted(path_dict["event_paths"])

    # check if the paths are empty
    if path_dict["yield_paths"] == []:
        raise SystemExit("No yield files found.")

    if path_dict["event_paths"] == []:
        raise SystemExit("No event files found.")

    return path_dict


def process_classified_events(path_classified_event_files: Dict[str, List[str]],
                              combination: str = 'SR',
                              correlation_method: str = 'jaccard',
                              correlation_matrix: str = 'global',
                              overlap_evolution: str = ""
                              ) -> Tuple[Dict[str, Any],
                                         np.ndarray,
                                         np.ndarray,
                                         np.ndarray,
                                         List[str]]:
    """
    Process classified events from SA.

    Args:
        path_classified_event_files (Dict[str,List[str]]): Dictionary with paths
            to classified event and yield files.
        combination (str): Combination of signal regions/analyses. Defaults to 'SR'.
            Also possible: 'Analysis'.
        correlation_method (str): Correlation method. Defaults to 'jaccard'.
            Also possible: 'jaccard', 'cosine', 'sorensen_dice', 'otsuka_ochiai', 'pearson', 
                szymkiewicz_simpson'.
        correlation_matrix (str): Correlation matrix type. Defaults to 'global'.
        overlap_evolution (str): Track evolution of the overlap matrix as events are sequentially added.

    Returns:
        Tuple[Dict[str, Any], np.ndarray, np.ndarray, List[str]]: Dictionary with yields info and when 
            selected 'local' also the correlation matrix with the number of events they shared, the accepted
            events in either of the two regions/anlyses and the column names of the correlation matrix.
    """
    # local correlation matrix (stays empty for global correlation matrix)
    local_correlation_matrix: np.ndarray = None  # type: ignore
    sum_x_y: np.ndarray = None  # type: ignore
    accepted_events_either: np.ndarray = None  # type: ignore

    # column names of the correlation matrix
    # TODO: if correlation matrix is global it needs to be read from somewhere
    # good idea: return it also from here
    column_names: List[str] = []

    # global correlation matrix
    # when dealing with the global correlation matrix,
    # it is only necessary to get the yields from the yield files
    # for calculating sensitivities/significances for signal regions
    # and analyses

    yield_dict: Dict[str, Dict] = {}

    # iterate over yield files and get the yields
    for path_idx, path_yield_file in enumerate(path_classified_event_files['yield_paths']):
        # get the model number
        model_number = path_yield_file.split('/')[-1].split('.')[0]

        # read the yield file with pandas
        df_yields: pd.DataFrame = pd.read_csv(
            path_yield_file,
            sep=",",
            header=0,
            index_col=False,
            usecols=[
                0,
                1])

        # get the maximum value of the events column
        total_events = df_yields['events'].max()

        # remove all rows <analysis_name>__All
        df_yields = df_yields[~df_yields['SR'].str.contains('__All')]

        # get all the analyses
        analyses = df_yields['SR'].str.split('__', expand=True)[0].unique()

        # iterate over all signal regions and put them in a dictionary where
        # the key is the analysis name and the value is list of signal regions
        # belonging to the analysis (will be just needed for local correlation
        # matrix)
        analysis_dict: Dict[str, List[str]] = {}
        for analysis in analyses:
            analysis_dict[analysis] = df_yields[df_yields['SR'].str.contains(
                analysis)].SR.values.tolist()
            # remove '<analysis_name>__' from the signal region names via list
            # comprehension
            analysis_dict[analysis] = [sr.split('__')[1]
                                       for sr in analysis_dict[analysis]]

        if combination == 'SR':
            # leave unchanged
            df_yields_signal_regions = df_yields

            # store in dictionary
            yield_dict[model_number] = {'df_yields': df_yields_signal_regions,
                                        'total_events': total_events}

        elif combination == 'Analysis':
            print(
                f" - ({path_idx+1}/{len(path_classified_event_files['yield_paths'])}) Doing Analysis Yields Combination")
            # group by analysis name
            df_yields_analysis = df_yields.groupby(
                df_yields['SR'].str.split('__', expand=True)[0]).sum()

            # take the index column and rename it to analysis and make it a
            # column
            df_yields_analysis['Analysis'] = df_yields_analysis.index
            df_yields_analysis.reset_index(drop=True, inplace=True)

            # rename the events column to combined_events
            df_yields_analysis.rename(
                columns={
                    'events': 'events'},
                inplace=True)

            # store in dictionary
            yield_dict[model_number] = {'df_yields': df_yields_analysis,
                                        'total_events': total_events}

    # local correlation matrix (the algorithm for the global correlation matrix is always run)
    # since the yield dict is needed in any case
    if correlation_matrix == 'local':
        print(" - Local Generation")
        # calculateion of local correlation matrix is necessary
        local_correlation_matrix, sum_x_y, accepted_events_either, column_names = __calculate_local_correlation_matrix(
            path_classified_event_files=path_classified_event_files,
            combination=combination,
            analysis_dict=analysis_dict,  # type: ignore
            correlation_method=correlation_method,
            overlap_evolution=overlap_evolution)

    return yield_dict, local_correlation_matrix, sum_x_y, accepted_events_either, column_names


def __stouffers_method(sensitivities: np.ndarray) -> float:
    """
    Calculate the combined sensitivity with Stouffer's method.

    Args:
        sensitivities (np.array): Array of sensitivities.

    Returns:
        float: Combined sensitivity.
    """
    # remove all NaNs
    sensitivities = sensitivities[~np.isnan(sensitivities)]

    # if length of sensitivities is 0 return 0
    if len(sensitivities) == 0:
        return 0

    # calculate the combined sensitivity with stouffers method
    return 1 / np.sqrt(sensitivities.shape[0]) * np.sum(sensitivities)


def __adding_method(sensitivities: np.ndarray) -> float:
    """
    Calculate the combined sensitivity with adding method.

    Args:
        sensitivities (np.array): Array of sensitivities.

    Returns:
        float: Combined sensitivity.
    """
    # remove all NaNs
    sensitivities = sensitivities[~np.isnan(sensitivities)]
    # if length of sensitivities is 0 return 0
    if len(sensitivities) == 0:
        return 0

    return sensitivities.sum()


def combine_yield_dicts(yield_dict: Dict[str, Dict],
                        combination: str = "SR",
                        combination_method: str = "stouffer") -> Dict[str, Any]:
    """
    Combine the yield dictionaries.

    Args:
        yield_dict (Dict[str, Dict]): Dictionary with yield data.
        combination (str): Combination method. Defaults to "SR".
            Also possible: "Analysis".
        combination_method (str): Combination method. Defaults to "stouffer".
            Also possible: "adding".

    Returns:
        Dict[str, Any]: Dictionary with combined yield data.
    """
    # combined yield dictionary
    combined_yield_dict: Dict[str, Any] = {}

    # model numbers into combined yield dictionary
    combined_yield_dict['model_numbers'] = list(yield_dict.keys())

    # combination function
    comb_func: Callable = None

    # if sensitivity is already evaluated combine them via
    # specified combination method
    if "sensitivity" in yield_dict[list(yield_dict.keys())[
            0]]["df_yields"].keys():
        if combination_method == 'stouffer':
            comb_func = __stouffers_method
        elif combination_method == 'adding':
            comb_func = __adding_method

        agg_kwargs = {'events': 'sum',
                      'sensitivity': comb_func}
    # if not defined yet, just sum up the events
    else:
        agg_kwargs = {'events': 'sum'}

    if combination == 'SR':
        # total events into combined yield dictionary
        combined_yield_dict['total_events'] = \
            [yield_dict[model_number]['total_events']
             for model_number in combined_yield_dict['model_numbers']]

        # combine dataframes of all models, sum up the events and keep the
        # signal region name
        combined_yield_dict['df_yields'] = \
            pd.concat([yield_dict[model_number]['df_yields']
                       for model_number in combined_yield_dict['model_numbers']],
                      ignore_index=True).groupby('SR').agg(agg_kwargs)

        # take the index column and rename it to signal region and make it a
        # column
        combined_yield_dict['df_yields']['SR'] = combined_yield_dict['df_yields'].index
        combined_yield_dict['df_yields'].reset_index(
            drop=True, inplace=True)

    else:
        # total events into combined yield dictionary
        combined_yield_dict['total_events'] = \
            [yield_dict[model_number]['total_events']
             for model_number in combined_yield_dict['model_numbers']]

        # combine dataframes of all models, sum up the events and keep the
        # analysis name
        combined_yield_dict['df_yields'] = \
            pd.concat([yield_dict[model_number]['df_yields']
                       for model_number in combined_yield_dict['model_numbers']],
                      ignore_index=True).groupby('Analysis').agg(agg_kwargs)

        # take the index column and rename it to analysis and make it a column
        combined_yield_dict['df_yields']['Analysis'] = combined_yield_dict['df_yields'].index
        combined_yield_dict['df_yields'].reset_index(
            drop=True, inplace=True)

    return combined_yield_dict


def __calculate_local_correlation_matrix(
        path_classified_event_files: Dict[str, List[str]],
        analysis_dict: Dict[str, List[str]],
        combination: str = "SR",
        correlation_method: str = "jaccard",
        overlap_evolution: str = "") -> Tuple[np.ndarray,
                                              np.ndarray,
                                              np.ndarray,
                                              List[str]]:
    """
    Function reads the data from the classified event root files and
    calculates the local correlation matrix.

    Args:
        path_classified_event_files (Dict[str,List[str]]): Dictionary with paths
            to classified event and yield files.
        analysis_dict (Dict[str,List[str]]): Dictionary with analysis names and
            signal region names.
        combination (str): Combination method. Defaults to "SR".
            Also possible: "Analysis".
        correlation_method (str): Correlation method. Defaults to "jaccard".
            Also possible: "pearson", "geometrical", "cosine", "otsuka_ochiai",
            "sorensen_dice", szymkiewicz_simpson".
        overlap_evolution (str): Path to folder where matrices are saved.

    Returns:
        Tuple[np.ndarray, np.ndarray, np.ndarray, List[str]]: Local correlation matrix, shared events between the signal region
            (needed for calculating statistics, see stats module), accepted events in either of the two
            signal regions/anlyses and the signal region/analysis names.
    """
    # for calculating the pearson correlation coefficient when iterating over
    # mulitple files that can only be read in chunks one needs to keep track of
    # calculated properties of the previous chunks and update them with the
    # current chunk. The chunks are in this case the root files.

    # 1. term for pearson correlation coefficient -> sum(x*y)
    sum_x_y: np.ndarray
    # 2. term for pearson correlation coefficient -> sum(x)
    sum_x: np.ndarray
    # 3. term for pearson correlation coefficient -> sum(x**2)
    sum_x_2: np.ndarray
    # 4. term for pearson correlation coefficient -> n (number of events)
    n: np.ndarray

    # correct analysis dictionary (some singal regions are not available as
    # signal regions in the root files)
    # open the first root file
    event_file = ur.open(path_classified_event_files['event_paths'][0])
    # iterate over all analysis names in the analysis dictionary
    for analysis in analysis_dict.keys():
        signal_regions_event_file = event_file[analysis+'__ntuple'].keys()
        # compare the signal regions in the event file with the signal regions
        # in the analysis dictionary and only keep the signal regions that are
        # in both
        analysis_dict[analysis] = [signal_region for signal_region in
                                   analysis_dict[analysis]
                                   if signal_region in
                                   signal_regions_event_file]

    # total number of events
    n: int = 0

    # if overlap evolution should be stored at certain directory
    if overlap_evolution != "":
        # create directory if it does not exist
        if not os.path.exists(overlap_evolution):
            os.makedirs(overlap_evolution)
            # make a file where amount of events is stored per file
            with open(os.path.join(overlap_evolution, "n.txt"), "w") as f:
                f.write("")

    # for combination Analysis the root file can be compressed because
    # mulitple signal regions are from same analysis
    if combination == 'Analysis':
        # define the arrays for the terms of the pearson correlation
        # coefficient
        sum_x_y = np.zeros((len(analysis_dict.keys()),
                            len(analysis_dict.keys()))).astype(np.int32)
        sum_x = np.zeros(sum_x_y.shape[0]).astype(np.int32)
        sum_x_2 = np.zeros_like(sum_x).astype(np.int32)

        # assign column names for the correlation matrix
        column_names: List[str] = [
            analysis for analysis in analysis_dict.keys()]

        # iterate through all the root files
        for idx, root_file_path in enumerate(path_classified_event_files['event_paths']):
            print(
                f" - ({idx+1}/{len(path_classified_event_files['event_paths'])}) Doing Analysis Events Combination")
            # open the root file
            root_file = ur.open(root_file_path)

            # analysis arrays
            list_analysis_arrays: List[np.ndarray] = []

            # iterate through the analysis dictionary
            for analysis, signal_regions in analysis_dict.items():
                # append the analysis arrays to the list of analysis arrays
                # the analysis arrays are the events of the signal regions
                # of the analysis, combined into one array
                list_analysis_arrays.append(np.array(
                    root_file[analysis+'__ntuple'].arrays(library='pd')[signal_regions]).any(axis=1).astype(np.float32))

            # concatenate all analysis events such that they form a matrix with
            # the shape (number of events, number of analyses)
            analysis_events: np.ndarray = np.stack(list_analysis_arrays)

            # calculate the terms for the pearson correlation coefficient
            sum_x_y += analysis_events.dot(analysis_events.T).astype(np.int32)
            sum_x += analysis_events.sum(axis=1).astype(np.int32)
            # if the events are binary than the sum of the normal events is
            # equal to the sum of the squared events
            # sum_x_2 += (analysis_events ** 2).sum(axis=1).astype(np.int32)
            n += analysis_events.shape[1]

            if overlap_evolution != "":
                np.save(os.path.join(overlap_evolution,
                        f"sum_x_y_{idx}.npy"), sum_x_y)
                np.save(os.path.join(overlap_evolution,
                        f"sum_x_{idx}.npy"), sum_x)
                # write event number to file
                with open(os.path.join(overlap_evolution, "n.txt"), "a") as f:
                    f.write(f"{idx}: {n}\n")

    # for combination SR the root file can not be compressed
    elif combination == 'SR':
        # define the arrays for the terms of the pearson correlation
        # coefficient
        # the length of the arrays is the number of all signal regions from all
        # analyses (signal regions are in the analysis_dict as values in a
        # list)
        number_signal_regions = sum([len(signal_regions)
                                    for signal_regions in analysis_dict.values()])
        sum_x_y = np.zeros(
            (number_signal_regions, number_signal_regions)).astype(np.int32)
        sum_x = np.zeros(sum_x_y.shape[0]).astype(np.int32)
        sum_x_2 = np.zeros_like(sum_x).astype(np.int32)

        # assign column names for the correlation matrix
        column_names: List[str] = [f"{analysis}__{signal_region}" for analysis in analysis_dict.keys()
                                   for signal_region in analysis_dict[analysis]]

        # iterate through all the root files
        for root_file_path in path_classified_event_files['event_paths']:
            # open the root file
            root_file = ur.open(root_file_path)

            # signal region arrays
            list_signal_region_arrays: List[np.ndarray] = []

            # iterate through the analysis dictionary
            for analysis, signal_regions in analysis_dict.items():

                # append the signal region dataframe to the list
                list_signal_region_arrays.append(
                    np.array(root_file[analysis + '__ntuple'].arrays(library='pd')[signal_regions]))

            # stack/concate all signal region events that all share the same number of events but have different number of signal regions
            # in the form of a matrix with the shape (number of events, number of signal regions)
            signal_region_events_combined: np.ndarray = np.concatenate(
                list_signal_region_arrays, axis=1).T.astype(np.bool8).astype(np.float32)

            # delete list_signal_region_arrays
            del list_signal_region_arrays

            # calculate the terms for the pearson correlation coefficient
            sum_x += signal_region_events_combined.sum(axis=1).astype(np.int32)
            # if the events are binary than the sum of the normal events is
            # equal to the sum of the squared events
            # sum_x_2 += (signal_region_events_combined ** 2).sum(axis=1).astype(np.int32)
            n += signal_region_events_combined.shape[1]

            # indices for pseudo sorting the matrix
            indices_sr = np.arange(signal_region_events_combined.shape[0])

            # matrix multiplication of the signal region events with itself
            sum_x_y += np.matmul(signal_region_events_combined[indices_sr, :],
                                 signal_region_events_combined[indices_sr, :].T).astype(np.int32)

    # since values are binary the sum of the normal events is equal to the sum
    # of the squared events (getting outside of the loop)
    sum_x_2 = sum_x

    # dependent on the correlation method select the correct function to
    # calculate the correlation matrix
    corr_func: Callable = geometrical_index
    if correlation_method == 'pearson':
        corr_func = pearson_correlation
    elif correlation_method == 'geometrical':
        corr_func = geometrical_index
    elif correlation_method == 'jaccard':
        corr_func = jaccard_index
    elif correlation_method == 'sorensen_dice':
        corr_func = sorensen_dice_index
    elif correlation_method == 'otsuka_ochiai':
        corr_func = otsuka_ochiai_index
    elif correlation_method == 'szymkiewicz_simpson':
        corr_func = szymkiewicz_simpson_index
    elif correlation_method == 'cosine':
        corr_func = cosine_index

    # calculate the pearson correlation coefficient
    local_correlation_matrix = corr_func(n=n,
                                         sum_x_y=sum_x_y,
                                         sum_x=sum_x,
                                         sum_x_2=sum_x_2)

    # remove nan values from the correlation matrix
    # hence also from sum_x_y, and column_names.
    # nan value when the sum of the events is zero
    # (e.g. no events in the signal region)

    # get indices of 0 values in sum_x
    nan_indices = np.where(sum_x == 0)[0]

    # accepted events either in one of the signal regions/analyses
    # is simply the denominator of the geometric correlation (B + B.T - A)
    accepted_events_either = np.ones_like(sum_x_y) * sum_x + \
        np.array(np.ones_like(sum_x_y) * sum_x).T - sum_x_y

    # remove nan values from all return values
    local_correlation_matrix, sum_x_y, accepted_events_either, column_names = __remove_nan_values(correlation_matrix=local_correlation_matrix,
                                                                                                  sum_x_y=sum_x_y,
                                                                                                  column_names=column_names,
                                                                                                  accepted_events_either=accepted_events_either,
                                                                                                  nan_indices=nan_indices)

    return local_correlation_matrix, sum_x_y, accepted_events_either, column_names


def __remove_nan_values(correlation_matrix: np.ndarray,
                        sum_x_y: np.ndarray,
                        accepted_events_either: np.ndarray,
                        column_names: List[str],
                        nan_indices: np.ndarray) -> Tuple[np.ndarray,
                                                          np.ndarray,
                                                          np.ndarray,
                                                          List[str]]:
    """
    Remove nan correlations from the correlation matrix.

    Args:
        correlation_matrix (np.ndarray): Correlation matrix.
        sum_x_y (np.ndarray): Matrix with the sum of the x and y values.
        accepted_events_either (np.ndarray): Matrix with events that are accepted in either signal region.
        column_names (List[str]): List of column names.
        nan_indices (np.ndarray): Array with indices of the removed rows and columns.

    Returns:
        Tuple[np.ndarray, np.ndarray, np.ndarray, List[str]]: Correlation matrix without nan correlations,
            matrix with the sum of the x and y values without nan values, matrix with events
            that are accepted in either signal region without nan values, and list of column names
            without nan values.
    """
    # remove the rows and columns from the correlation matrix
    correlation_matrix = np.delete(correlation_matrix, nan_indices, axis=0)
    correlation_matrix = np.delete(correlation_matrix, nan_indices, axis=1)

    # remove the rows and columns from the sum_x_y matrix
    sum_x_y = np.delete(sum_x_y, nan_indices, axis=0)
    sum_x_y = np.delete(sum_x_y, nan_indices, axis=1)

    # remove the rows and columns from the accepted_events_either matrix
    accepted_events_either = np.delete(
        accepted_events_either, nan_indices, axis=0)
    accepted_events_either = np.delete(
        accepted_events_either, nan_indices, axis=1)

    # remove the indices from the column names
    column_names = [column_names[i]
                    for i in range(len(column_names)) if i not in nan_indices]

    return correlation_matrix, sum_x_y, accepted_events_either, column_names
