"""
Functions to extract sensitivities from the dataframe.
"""

from typing import Dict, List, Any, Union


def extract_sensitivities(yield_dict: Dict[str, Any],
                          combination_names: List[str],
                          combination: str = 'SR',) -> Dict[str, Union[float, List[float]]]:
    """
    Extracts from the yield dictionary and the corresponding dataframe the sensitivities
    and returns them as a dictionary.

    Args:
        yield_dict (Dict[str, Any]): Yield dictionary.
        combination_names (List[str]): List of combination names (SR, Analysis).
        combination (str, optional): Combination of the signal regions or analyses. Defaults to 'SR'.
            Also possible is 'Analysis'.

    Returns:
        Dict[str, Union[float, List[float]]]: Dictionary with the sensitivities. When
            the yield dictionary is combined, the sensitivities are returned as a float.
            When the yield dictionary is not combined, the sensitivities are returned
            as a list of floats.
    """
    # create an empty dictionary for the sensitivities
    sensitivity_dict: Dict[str, Union[float, List[float]]] = {}
    # check if the yield dictionary is combined or not ('model_numbers' key
    # must be list).
    if 'model_numbers' in yield_dict.keys():
        # check if the sensitivity value is in the dataframe
        if not 'sensitivity' in yield_dict['df_yields'].columns:
            raise ValueError("The sensitivity value is not in the dataframe.\n"
                             "First calculate the sensitivity value for each signal region.")

        else:
            # extract the sensitivities from the dataframe, for the corresponding
            # combination names in the combination column
            sensitivity_dict = {combination_name: yield_dict['df_yields'].loc[yield_dict['df_yields'][combination] == combination_name,
                                                                              'sensitivity'].values[0] for combination_name in combination_names}
    else:
        # check if the sensitivity value is in the dataframe
        if not 'sensitivity' in yield_dict[list(yield_dict.keys())[0]]['df_yields'].columns:
            raise ValueError("The sensitivity value is not in the dataframe.\n"
                             "First calculate the sensitivity value for each signal region.")
        else:
            # extract the sensitivities from the dataframe, for the corresponding
            # combination names in the combination column
            # must be done for all model points
            sensitivity_dict = {combination_name: [yield_dict[model_number]['df_yields'].loc[yield_dict[model_number]['df_yields'][combination] == combination_name,
                                                                                             'sensitivity'].values[0]
                                                   for model_number in yield_dict.keys()]
                                for combination_name in combination_names
                                }

    return sensitivity_dict
