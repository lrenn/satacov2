"""
Processing of info files.
"""

import os
from typing import Dict, List

import pandas as pd


def process_info_files(
        path_info_files: str) -> Dict[str, Dict]:
    """Function to process info files.

    Args:
        path_info_files (str): Path to info files.

    Returns:
        Dict [str, Dict]: Dictionary with info files.

    Notes:
        The returned dictionary has the following structure
        {<analysis_name>@<signal_region_name>:
            {
                "Analysis": <analysis_name>,
                "SR": <signal_region_name>,
                "nBkg": <Number of expected bkg events.>,
                "errBkg": <Error of expected bkg events.>,
                "nData": <Number of expected points>
            }
        }
    """
    # list all info files in the directory
    info_files: List[str] = os.listdir(path_info_files)

    # initialize directory with info
    info_dict: Dict[str, Dict] = {}

    # iterate through info files and
    for file in info_files:
        if file.endswith(".info"):
            df_info = pd.read_csv(path_info_files + "/" + file)
            for _, row in df_info.iterrows():
                info_dict[file.strip(".info") + "__" + row["SR"]] = {  # type: ignore
                    "Analysis": file.strip(".info"),
                    "SR": row["SR"],
                    "nBkg": row["nBkg"],
                    "errBkg": row["errBkg"],
                    "nData": row["nData"]}

    return info_dict
