"""
Filters of the overlap matrix.
"""

from typing import Any, Dict, List, Tuple

import numpy as np


def apply_substring_filter(matrix: np.ndarray,
                           column_names: List[str],
                           substring: List[str] = ["CR"]) -> Tuple[np.ndarray, List[str]]:
    """
    Filter that removes all columns that include a certain substring 
    (Default is "CR").

    Args:
        matrix (np.ndarray): Overlap matrix.
        column_names (List[str]): List of the column names.
        substring (List[str]): List of substrings that will be removed from matrix.

    Returns:
        (Tuple[np.ndarray, List[str]]): Matrix and new column names.
    """
    # copy matrix
    removed_substring_matrix: np.ndarray = matrix.copy()

    # indices to remove
    remove_indices: List[int] = []

    # iterate over the columns
    remove_indices = [idx for idx, name in enumerate(
        column_names) for sub in substring if sub in name]

    # remove the columns from the matrix
    removed_substring_matrix = np.delete(
        removed_substring_matrix, remove_indices, axis=0)
    removed_substring_matrix = np.delete(
        removed_substring_matrix, remove_indices, axis=1)

    # remove the column names
    column_names = [name for idx, name in enumerate(
        column_names) if idx not in remove_indices]

    return removed_substring_matrix, column_names


def apply_LH_expansion_filter(matrix: np.ndarray,
                              column_names: List[str],
                              LH_column_names: List[str]) -> np.ndarray:
    """
    Filter expands the matrix to having the likelihood scans in blocks. 

    Args:
        matrix (np.ndarray): Overlap matrix.
        column_names (List[str]): List of the column names. Format: <analysis_name>__<signal_region>.
        LH_column_names (List[str]): List of the likelihood scan column names. Format: <analysis_name>__<likelihood_scan>__<signal_region>.

    Returns:
        (np.ndarray): Expanded matrix.
    """
    ana__lhs: Dict[str, List[str]] = {}
    for LH_column in LH_column_names:
        ana__lh = LH_column.split("__")[0]+"__"+LH_column.split("__")[1]
        ana__sr = LH_column.split("__")[0]+"__"+LH_column.split("__")[2]
        if ana__lh not in ana__lhs:
            ana__lhs[ana__lh] = [column_names.index(ana__sr)]
        else:
            ana__lhs[ana__lh].append(column_names.index(ana__sr))

        ana__lhs[ana__lh] = list(sorted(set(ana__lhs[ana__lh])))

    expanded_shape = sum([len(ana__lhs[ana__lh]) for ana__lh in ana__lhs])
    expand_matrix = np.zeros((expanded_shape, expanded_shape))

    # Iterate over the lh blocks and copy the indices to the expanded matrix
    column: int = 0
    row: int = 0
    for ana__lh_i, index_block_i in ana__lhs.items():
        for ana__lh_j, index_block_j in ana__lhs.items():
            block = matrix[np.ix_(index_block_i, index_block_j)]
            expand_matrix[row:row+block.shape[0],
                          column:column+block.shape[1]] = block
            column += block.shape[1]
        row += block.shape[0]
        column = 0

    return expand_matrix


def apply_LH_reduction_filter(matrix: np.ndarray,
                              column_names: List[str],
                              LH_column_names: List[str]) -> np.ndarray:
    """
    Reverse filter of the LH expansion filter.

    Args:
        matrix (np.ndarray): Overlap matrix.
        column_names (List[str]): List of the column names. Format: <analysis_name>__<signal_region>.
        LH_column_names (List[str]): List of the likelihood scan column names. Format: <analysis_name>__<likelihood_scan>__<signal_region>.

    Returns:
        (np.ndarray): Reduced matrix.
    """
    # Copy matrix
    reduced_matrix: np.ndarray = matrix.copy()

    # Iterate over the column names
    for column_name in column_names:
        # Split the column name
        column_name_split: List[str] = column_name.split('__')

        remove_indices: List[int] = []

        # Get the index of the LH column that fullfills the condition
        # LH_column_names[idx].split('__')[0] == column_name_split[0] and LH_column_names[idx].split('__')[2] == column_name_split[1]
        for idx_LH, LH_column_name in enumerate(LH_column_names):
            if LH_column_name.split('__')[0] == column_name_split[0] and LH_column_name.split('__')[2] == column_name_split[1]:
                remove_indices.append(idx_LH)

        # Mark the indices to remove with a special value (e.g., 'X')
        if len(remove_indices) - 1 > 0:
            for inserted_index in remove_indices[1:]:
                reduced_matrix[inserted_index, :] = np.pi
                reduced_matrix[:, inserted_index] = np.pi

    # Remove all rows and columns marked with the special value
    reduced_matrix = reduced_matrix[~(reduced_matrix == np.pi).all(axis=1)]
    reduced_matrix = reduced_matrix[:, ~(reduced_matrix == np.pi).all(axis=0)]

    # sort again otherwise wrong indices
    current_order = []
    for column_name in LH_column_names:
        if column_name.split('__')[0] + '__' + column_name.split('__')[2] in current_order:
            continue
        else:
            current_order.append(column_name.split(
                '__')[0] + '__' + column_name.split('__')[2])
    order = []
    for column_name in column_names:
        order.append(current_order.index(column_name))
    reduced_matrix = reduced_matrix[np.ix_(order, order)]

    return reduced_matrix


def apply_CR_filter(matrix: np.ndarray,
                    expand_matrix: np.ndarray,
                    column_names: List[str],
                    LH_column_names: List[str]) -> np.ndarray:
    """
    Function that checks wether the CRs of different likelihood scans are overlapping.
    If they are overlapping, the SRs are from the same combination are as well marked as overlapping.

    Args:
        matrix (np.ndarray): Overlap matrix or upper limit matrix.
        expand_matrix (np.ndarray): Expanded matrix.
        column_names (List[str]): List of the column names.
        LH_column_names (List[str]): List of the likelihood scan column names.

    Returns:
        (np.ndarray): Indices of the combinations that are overlapping.
    """
    # Get the blocks of the matrix
    ana__lhs: Dict[str, Dict[str, int]] = {}

    # Copy matrix to expanded matrix
    cr_filtered_matrix = expand_matrix.copy()

    for LH_column in LH_column_names:
        ana__lh = LH_column.split("__")[0]+"__"+LH_column.split("__")[1]
        ana__sr = LH_column.split("__")[0]+"__"+LH_column.split("__")[2]
        if ana__lh not in ana__lhs:
            ana__lhs[ana__lh] = {ana__sr: column_names.index(ana__sr)}
        else:
            ana__lhs[ana__lh][ana__sr] = column_names.index(ana__sr)

        ana__lhs[ana__lh] = dict(
            sorted(ana__lhs[ana__lh].items(), key=lambda item: item[1]))

    # Iterate over the blocks
    column: int = 0
    row: int = 0
    for ana__lh_i, index_block_i in ana__lhs.items():
        for ana__lh_j, index_block_j in ana__lhs.items():
            # Check if there are any CRs that are overlapping
            max_CR_value = 0.0
            if ana__lh_i != ana__lh_j:
                for ana__sr_i, index_i in index_block_i.items():
                    if "CR" in ana__sr_i:
                        for ana__sr_j, index_j in index_block_j.items():
                            if "CR" in ana__sr_j:
                                max_CR_value = max(
                                    max_CR_value, matrix[index_i, index_j])

            # Get the block from the matrix
            block = matrix[np.ix_(
                list(index_block_i.values()), list(index_block_j.values()))]

            # The entries in the matrix should be set to max_CR_value if they are lower, otherwise leave them as they are
            block[block < max_CR_value] = max_CR_value

            # The blocks should be set to the expanded matrix
            cr_filtered_matrix[row:row+block.shape[0],
                               column:column+block.shape[1]] = block
            column += block.shape[1]
        row += block.shape[0]
        column = 0

    return cr_filtered_matrix


def reassign_CR_values_from_same_analysis(
        filtered_matrix: np.ndarray,
        matrix: np.ndarray,
        column_names: List[str]) -> np.ndarray:
    """
    Function that reassigns the CR values from the same analysis.

    Args:
        filtered_matrix (np.ndarray): Filtered matrix.
        matrix (np.ndarray): Overlap matrix, upper limit matrix or sufficient statistics matrix.
        column_names (List[str]): List of the column names.

    Returns:
        (np.ndarray): Reassigned matrix.
    """
    # copy matrix
    reassigned_matrix: np.ndarray = filtered_matrix.copy()

    # analysis and corresponding indices of all control regions
    analysis_CRs_indices: Dict[str, List[int]] = {}

    analysis_indices: Dict[str, List[int]] = {}

    analysis = ""

    # get the indices of all control regions per analysis
    for idx, column_name in enumerate(column_names):
        if "CR" in column_name:
            analysis = column_name.split("__")[0]
            if analysis not in analysis_CRs_indices:
                analysis_CRs_indices[analysis] = [idx]
            else:
                analysis_CRs_indices[analysis].append(idx)

        # get the indices of all signal regions per analysis
        else:
            analysis = column_name.split("__")[0]
            if analysis not in analysis_indices:
                analysis_indices[analysis] = [idx]
            else:
                analysis_indices[analysis].append(idx)

    # iterate over the analyses
    for analysis, indices in analysis_CRs_indices.items():
        # iterate over the control regions
        reassigned_matrix[analysis_CRs_indices[analysis], min(
            analysis_indices[analysis]):max(analysis_indices[analysis])+1] = matrix[analysis_CRs_indices[analysis], min(analysis_indices[analysis]):max(analysis_indices[analysis])+1]
        reassigned_matrix[min(analysis_indices[analysis]):max(analysis_indices[analysis])+1,
                          analysis_CRs_indices[analysis]] = matrix[min(analysis_indices[analysis]):max(analysis_indices[analysis])+1, analysis_CRs_indices[analysis]]

    return reassigned_matrix
