"""
Test for plotting distributions.
"""

import os
import sys
from typing import Any, Dict, List

import numpy as np
import pandas as pd
import pytest

from SATACO.plotting_tools.plot_distributions import (
    bar_plot_acccepted_events, plot_allowed_combinations_threshold,
    plot_low_acceptance_distributions, plot_multiple_indices_distributions,
    plot_overlap_distribution, plot_path_distributions)

DIRNAME = os.path.dirname(__file__)

# Append the project root directory to the Python path
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))


def test_plot_overlap_distribution():
    """
    Test the plot_overlap_distribution function.
    """
    # Create a random matrix
    matrix = np.random.uniform(0, 1, (10, 10))
    matrix = (matrix + matrix.T)/2
    upper_limit = matrix * 1.1
    np.fill_diagonal(matrix, 1)

    # Test the function
    plot_overlap_distribution(
        overlap_matrix=matrix,
        upper_limit=upper_limit,
        save=DIRNAME + "/test_plot_overlap_distribution.png")

    # test if file is created in this directory
    assert os.path.isfile(DIRNAME +
                          "/test_plot_overlap_distribution.png")

    # remove the file
    os.remove(DIRNAME +
              "/test_plot_overlap_distribution.png")


def test_plot_allowed_combinations_threshold():
    """
    Test the plot_allowed_combinations_threshold function.
    """
    # Create a random matrix
    matrix = np.random.uniform(0, 1, (40, 40))
    matrix = (matrix + matrix.T)/2
    upper_limit = matrix * 1.1
    np.fill_diagonal(matrix, 1)

    # Test the function
    plot_allowed_combinations_threshold(
        overlap_matrix=matrix,
        upper_limit=upper_limit,
        save=DIRNAME + "/test_plot_allowed_combinations_threshold.png")

    # test if file is created in this directory
    assert os.path.isfile(DIRNAME +
                          "/test_plot_allowed_combinations_threshold.png")

    # remove the file
    os.remove(DIRNAME +
              "/test_plot_allowed_combinations_threshold.png")


def test_plot_multiple_indices_distributions():
    """
    Test the plot_multiple_indices_distributions function.
    """
    overlap_values = [np.random.uniform(0, 1, 100).tolist(),
                      np.random.uniform(0, 1, 100).tolist(),
                      np.random.uniform(0, 1, 100).tolist(),
                      np.random.uniform(0, 1, 100).tolist(),
                      np.random.uniform(0, 1, 100).tolist(),
                      np.random.uniform(0, 1, 100).tolist()]

    index_names_list = [["index0", "index1"],
                        ["index0", "index2"],
                        ["index0", "index3"],
                        ["index0", "index4"],
                        ["index0", "index5"],
                        ["index0", "index7"]]
    plot_multiple_indices_distributions(
        overlap_values_list=overlap_values,
        index_names_list=index_names_list,
        save=DIRNAME + "/test_plot_multiple_indices_distributions.png")
    assert os.path.isfile(DIRNAME +
                          "/test_plot_multiple_indices_distributions.png")

    # remove the file
    os.remove(DIRNAME +
              "/test_plot_multiple_indices_distributions.png")


def test_plot_low_acceptance_distributions():
    """
    Test the plot_low_acceptance_distributions
    """
    np.random.seed(42)
    # Sample data for testing
    efficiencies_data: Dict[str, List[Any]] = {
        "Model": [1, 2, 3],
        "SR1": [0, 1, 1],
        "SR2": [0, 0, 1],
        "n": [100000, 95000, 90000]
    }

    parameters_data: Dict[str, List[Any]] = {
        "Model": [1, 2, 3],
        "M_1": np.random.uniform(0, 1, 3).tolist(),
        "tanb": np.random.uniform(0, 1, 3).tolist(),
        'M_2': np.random.uniform(0, 1, 3).tolist(),
        'M_3': np.random.uniform(0, 1, 3).tolist(),
        'At': np.random.uniform(0, 1, 3).tolist(),
        'Ab': np.random.uniform(0, 1, 3).tolist(),
        'Atau': np.random.uniform(0, 1, 3).tolist(),
        'mu': np.random.uniform(0, 1, 3).tolist(),
        'mA': np.random.uniform(0, 1, 3).tolist(),
        'meL': np.random.uniform(0, 1, 3).tolist(),
        'mmuL': np.random.uniform(0, 1, 3).tolist(),
        'mtauL': np.random.uniform(0, 1, 3).tolist(),
        'meR': np.random.uniform(0, 1, 3).tolist(),
        'mmuR': np.random.uniform(0, 1, 3).tolist(),
        'mtauR': np.random.uniform(0, 1, 3).tolist(),
        'mqL1': np.random.uniform(0, 1, 3).tolist(),
        'mqL2': np.random.uniform(0, 1, 3).tolist(),
        'mqL3': np.random.uniform(0, 1, 3).tolist(),
        'muR': np.random.uniform(0, 1, 3).tolist(),
        'mcR': np.random.uniform(0, 1, 3).tolist(),
        'mtR': np.random.uniform(0, 1, 3).tolist(),
        'mdR': np.random.uniform(0, 1, 3).tolist(),
        'msR': np.random.uniform(0, 1, 3).tolist(),
        'mbR': np.random.uniform(0, 1, 3).tolist()}

    efficiencies_df: pd.DataFrame = pd.DataFrame(efficiencies_data)
    parameters_df: pd.DataFrame = pd.DataFrame(parameters_data)

    # load the test data
    try:
        efficiencies_df = pd.read_csv(os.path.join(os.path.dirname(__file__),
                                                   "pmssm_events.csv"), skiprows=1)
        parameters_df = pd.read_csv(os.path.join(os.path.dirname(__file__),
                                                 "pmssm_params.csv"), skiprows=1)
    except FileNotFoundError:
        print("Test data not found. Run with local data.")
        pass

    plot_low_acceptance_distributions(
        efficiencies_df=efficiencies_df,
        parameters_df=parameters_df,
        save=DIRNAME + "/test_plot_low_acceptance_distributions.png")

    assert os.path.isfile(DIRNAME +
                          "/test_plot_low_acceptance_distributions.png")

    # remove the file
    os.remove(DIRNAME +
              "/test_plot_low_acceptance_distributions.png")


def test_bar_plot_acccepted_events():
    """
    Test the bar_plot_acccepted_events function.
    """
    # dictionary with the number of accepted events per analysis
    accepted_events = {"Analysis0__SR1": 100,
                       "Analysis0__SR2": 200,
                       "Analysis1__SR1": 300,
                       "Analysis1__SR2": 400,
                       "Analysis2__SR1": 500,
                       "Analysis2__SR2": 600}

    # Test the function
    bar_plot_acccepted_events(
        accepted_events=accepted_events,
        save=DIRNAME + "/test_bar_plot_acccepted_events.png")

    # test if file is created in this directory
    assert os.path.isfile(DIRNAME +
                          "/test_bar_plot_acccepted_events.png")

    # remove the file
    os.remove(DIRNAME +
              "/test_bar_plot_acccepted_events.png")

    # define a focus analysis
    focus_analysis = "Analysis1"

    # Test the function
    bar_plot_acccepted_events(
        accepted_events=accepted_events,
        focus_analysis=focus_analysis,
        save=DIRNAME + "/test_bar_plot_acccepted_events_focus.png")

    # test if file is created in this directory
    assert os.path.isfile(DIRNAME +
                          "/test_bar_plot_acccepted_events_focus.png")

    # remove the file
    os.remove(DIRNAME +
              "/test_bar_plot_acccepted_events_focus.png")

    # define a focus analysis ( that is not in the dictionary)
    focus_analysis = "Analysis3"

    # Test the function
    with pytest.raises(KeyError):
        bar_plot_acccepted_events(
            accepted_events=accepted_events,
            focus_analysis=focus_analysis,
            save=DIRNAME + "/test_bar_plot_acccepted_events_focus.png")


def test_plot_path_distributions():
    """
    Test the plot_path_distributions function.
    """
    paths = ["A&B&C", "A&B&D", "A&B&C", "A&B&C",
             "A&B&C", "A", "B", "A&B&C", "A&B&D", "A&C&B"]
    path_lengths = [3, 3, 3, 3, 3, 1, 1, 3, 3, 0]

    # Test the function
    plot_path_distributions(
        paths=paths,
        path_lengths=path_lengths,
        log_scale=True,
        save=DIRNAME + "/test_plot_path_distributions.png")

    # test if file is created in this directory
    assert os.path.isfile(DIRNAME +
                          "/test_plot_path_distributions.png")

    # remove the file
    os.remove(DIRNAME +
              "/test_plot_path_distributions.png")
