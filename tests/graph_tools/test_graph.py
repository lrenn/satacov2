"""
Testing the graph functions.
"""

from SATACO.graph_tools.graph import Graph
import sys
import os

# Append the project root directory to the Python path
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))


def test_add_weighted_edges():
    """
    Testing the add_weighted_edges function.
    """
    # empty list of edges
    graph = Graph()
    graph.add_weighted_edges([])
    assert graph.edges() == []

    # non-empty list of edges
    graph = Graph()
    graph.add_weighted_edges([(0, 1, 0.5), (1, 2, 0.3), (0, 2, 0.2)])
    assert graph.edges() == [(0, 1), (0, 2), (1, 2)]


def test_edges():
    """
    Testing the edges function.
    """
    # no source node specified
    graph = Graph()
    graph.add_weighted_edges([(0, 1, 0.5), (1, 2, 0.3), (0, 2, 0.2)])
    assert graph.edges() == [(0, 1), (0, 2), (1, 2)]

    # source node specified
    assert graph.edges(0) == [(0, 1), (0, 2)]

    # invalid source node specified
    assert graph.edges(3) == []
