"""
Test path finder searching for the longest path.
"""
from SATACO.graph_tools.path_finder import PathFinder
import numpy as np
import sys
import os
import pytest

# Append the project root directory to the Python path
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))


def test_find_longest_path():
    """
    Testing the find_longest_path function.
    """
    correlation_matrix = np.array([
        [1.0, 0.05, 0.03, 0.02, 1.0],
        [0.05, 1.0, 0.04, 0.03, 1.0],
        [0.03, 0.04, 1.0, 0.05, 1.0],
        [0.02, 0.03, 0.05, 1.0, 1.0],
        [1.0, 1.0, 1.0, 1.0, 1.0]
    ])
    # path finder with uniform weights
    path_finder_01 = PathFinder(correlations=correlation_matrix,
                                threshold=0.035)

    # path finder with weights
    path_finder_02 = PathFinder(correlations=correlation_matrix,
                                threshold=0.04,
                                weights=[0.1, 0.2, 0.3, 0.4, 0.1])
    
    # run path finder
    path_01 = path_finder_01.find_path(top=1)

    # run path finder
    path_02 = path_finder_02.find_path(top=1)


    # check if path is correct
    assert path_01[0]['path'] == [0, 2]
    assert np.isclose(path_01[0]['weight'], 2.0)
    assert path_02[0]['path'] == [1, 3]
    assert np.isclose(path_02[0]['weight'], 0.6)
