"""
Test the statistics functions for the correlation matrix.
"""

from SATACO.stats_tools.stats import calculate_statistics_of_correlations
from SATACO.processing_tools.process_overlaps import geometrical_index
import numpy as np
import pytest
import sys
import os

# Append the project root directory to the Python path
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))


def test_calculate_statistics_of_correlations():
    """
    Test the function calculate_statistics_of_correlations.
    """
    # matrix of shared events
    shared_events = np.array([[100, 3, 0, 30],
                              [3, 100, 0, 9],
                              [0, 0, 0, 0],
                              [30, 9, 0, 100]])

    # threshold
    threshold = 0.1  # 10%

    # significance level
    confidence_level = 0.95

    # true significant correlations
    true_significant_correlations = np.array([[1,  0, -1,  1],
                                              [0,  1, -1, -1],
                                              [-1, -1, 1, -1],
                                              [1, -1, -1,  1]])

    # true upper limit (diagonal is always set to 1)
    true_upper_p_values = np.array([[1., 0.07497892, 0.95, 0.38065005],
                                    [0.07497892, 1., 0.95, 0.15034398],
                                    [0.95, 0.95, 1., 0.95],
                                    [0.38065005, 0.15034398, 0.95, 1.]])

    # calculate significant correlations
    significant_correlations, up = calculate_statistics_of_correlations(shared_events,
                                                                        threshold,
                                                                        confidence_level,
                                                                        correlation_method='szymkiewicz_simpson')

    # assert
    assert np.allclose(significant_correlations,
                       true_significant_correlations)
    assert np.allclose(up, true_upper_p_values)

    # calculate significant correlations
    shared_events = np.array([[100, 0, 3],
                              [0, 100, 0],
                              [3, 0, 100]])
    significant_correlations, up = calculate_statistics_of_correlations(shared_events,
                                                                        threshold,
                                                                        confidence_level,
                                                                        correlation_method='geometrical')

    # true significant correlations
    true_significant_correlations = np.array([[1, 0, 0],
                                              [0, 1, 0],
                                              [0, 0, 1]])
    # true upper limit
    true_upper_p_values = np.array([[0.99949227, 0.01479362, 0.3869202],
                                    [0.01479362, 0.99949227, 0.01479362],
                                    [0.3869202, 0.01479362, 0.99949227]])

    # assert
    assert np.allclose(significant_correlations,
                       true_significant_correlations)
    # assert np.allclose(up, true_upper_p_values, rtol=1e-2)

    # test if exception is raised if correlation method is not implemented
    with pytest.raises(NotImplementedError):
        calculate_statistics_of_correlations(shared_events,
                                             threshold,
                                             confidence_level,
                                             correlation_method='not_implemented')
