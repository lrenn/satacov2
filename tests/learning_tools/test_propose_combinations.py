"""
Test the function propose_combinations.
"""

import sys
import os
import pytest
import numpy as np
from SATACO.learning_tools.propose_combinations import propose_combinations

# Append the project root directory to the Python path
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))


def test_propose_combinations():
    """
    Test propose_combinations.
    """
    # true column names
    column_names = ['SR1', 'SR2', 'SR3']

    # significant correlations matrix
    significant_correlations = np.array([[0, 1, -1], [1, 0, 1], [-1, 1, 0]])

    # true proposed combinations
    proposed_combinations_true = {'SR3&SR1': '2&0'}

    # proposed combinations
    proposed_combinations = propose_combinations(significant_correlations,
                                                 column_names)

    assert proposed_combinations == proposed_combinations_true

    with pytest.raises(ValueError):
        proposed_combinations = propose_combinations(
            significant_correlations,
            column_names[:-1])
