"""
Test the overlap functions defined for the SATACO package.
"""

import os
import sys

import numpy as np

# from SATACO.processing_tools.process_overlaps import otsuka_ochiai_index # same as cosine_index
# from SATACO.processing_tools.process_overlaps import jaccard_index # same as geometrical_index
from SATACO.processing_tools.process_overlaps import (
    cosine_index, geometrical_index, pearson_correlation, sorensen_dice_index,
    szymkiewicz_simpson_index)

# Append the project root directory to the Python path
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))


def test_pearson_correlation():
    """
    Test the pearson correlation function
    """
    # arrays for testing (simulating 2 signal regions and 5 events)
    array_0 = np.array([1, 1, 1, 0, 1])
    array_1 = np.array([1, 1, 0, 1, 0])

    # get arguments for the function
    n = len(array_0)
    sum_x = np.array([np.sum(array_0), np.sum(array_1)])
    # make array_0 and array_1 a matrix with 2 rows and 5 columns
    mat_0 = np.array([array_0, array_1])
    sum_x_y = mat_0 @ mat_0.T

    sum_x_2 = np.array([np.sum(array_0**2), np.sum(array_1**2)])

    true_correlation = np.corrcoef(mat_0)

    # calculate the correlation
    correlation = pearson_correlation(sum_x=sum_x,
                                      sum_x_y=sum_x_y,
                                      sum_x_2=sum_x_2,
                                      n=n)
    assert np.allclose(correlation, true_correlation, atol=1e-4)


def test_geometrical_correlation():
    """
    Test the geometrical correlation function
    """
    # arrays for testing (simulating 2 signal regions and 5 events)
    array_0 = np.array([1, 1, 1, 0, 1])
    array_1 = np.array([1, 1, 0, 1, 0])

    # get arguments for the function
    sum_x = np.array([np.sum(array_0), np.sum(array_1)])
    # make array_0 and array_1 a matrix with 2 rows and 5 columns
    mat_0 = np.array([array_0, array_1])
    sum_x_y = mat_0 @ mat_0.T

    # calculate the correlation
    correlation = geometrical_index(sum_x=sum_x,
                                    sum_x_y=sum_x_y)
    true_correlation = np.array([[1., 0.4],
                                 [0.4, 1.]])
    assert np.allclose(correlation, true_correlation, atol=1e-4)


def test_sorenson_dice_index():
    """
    Test the sorensen dice index function
    """
    # arrays for testing (simulating 2 signal regions and 5 events)
    array_0 = np.array([1, 1, 1, 0, 1])
    array_1 = np.array([1, 1, 0, 1, 0])

    # get arguments for the function
    sum_x_2 = np.array([np.sum(array_0**2), np.sum(array_1**2)])
    # make array_0 and array_1 a matrix with 2 rows and 5 columns
    mat_0 = np.array([array_0, array_1])
    sum_x_y = mat_0 @ mat_0.T

    # calculate the correlation
    correlation = sorensen_dice_index(sum_x_2=sum_x_2,
                                      sum_x_y=sum_x_y)

    # true correlation
    true_correlation = np.array([[1., 4 / 7],
                                 [4 / 7, 1.]])

    assert np.allclose(correlation, true_correlation, atol=1e-4)


def test_szymkiewicz_simpson_index():
    """
    Test the szymkiewicz simpson index function.
    """
    # arrays for testing (simulating 2 signal regions and 5 events)
    array_0 = np.array([1, 1, 1, 0, 1])
    array_1 = np.array([1, 1, 0, 1, 0])

    # get arguments for the function
    sum_x = np.array([np.sum(array_0), np.sum(array_1)])

    # make array_0 and array_1 a matrix with 2 rows and 5 columns
    mat_0 = np.array([array_0, array_1])
    sum_x_y = mat_0 @ mat_0.T

    # calculate the correlation
    correlation = szymkiewicz_simpson_index(sum_x=sum_x,
                                            sum_x_y=sum_x_y)

    # true correlation
    true_correlation = np.array([[1., 2 / 3],
                                 [2 / 3, 1.]])

    assert np.allclose(correlation, true_correlation, atol=1e-4)


def test_cosine_index():
    """
    Test the cosine similarity function.
    """
    # arrays for testing (simulating 2 signal regions and 5 events)
    array_0 = np.array([1, 1, 1, 0, 1])
    array_1 = np.array([1, 1, 0, 1, 0])

    # get arguments for the function
    sum_x = np.array([np.sum(array_0), np.sum(array_1)])
    # make array_0 and array_1 a matrix with 2 rows and 5 columns
    mat_0 = np.array([array_0, array_1])
    sum_x_y = mat_0 @ mat_0.T

    # calculate the correlation
    correlation = cosine_index(sum_x=sum_x,
                               sum_x_y=sum_x_y)

    # true correlation
    true_correlation = np.array([[1., 2 / np.sqrt(3 * 4)],
                                 [2 / np.sqrt(3 * 4), 1.]])

    assert np.allclose(correlation, true_correlation, atol=1e-4)
