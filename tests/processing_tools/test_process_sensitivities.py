"""
Testing functions from processing sensivities.
"""

from SATACO.processing_tools.process_sensitivities import unit_sensitivity
from SATACO.processing_tools.process_sensitivities import gaussian_approximation_wo_errors
from SATACO.processing_tools.process_sensitivities import gaussian_approximation_w_errors
from SATACO.processing_tools.process_sensitivities import recommended_sensitivity_estimation

import numpy as np
from typing import Dict
import sys
import os

# Append the project root directory to the Python path
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))


def test_unit_sensitivity():
    """
    Test unit sensitivity.
    """
    assert unit_sensitivity() == 1.0


def test_gaussian_approximation_wo_errors():
    """
    Test gaussian approximation without errors.
    """
    # init kwargs
    kwargs: Dict[str, float] = {"signal": 1.0,
                                "background": 4.0}
    assert gaussian_approximation_wo_errors(**kwargs) == 0.5


def test_gaussian_approximation_w_errors():
    """
    Test gaussian approximation with errors.
    """
    # init kwargs
    kwargs: Dict[str, float] = {"signal": 3.0,
                                "background": 5.0,
                                "background_error": 2.0}
    assert gaussian_approximation_w_errors(**kwargs) == 1.0


def test_recommended_sensitivity_estimation():
    """
    Test recommended sensitivity estimation.
    """
    # init kwargs
    kwargs: Dict[str, float] = {"signal": 0,
                                "background": 10,
                                "background_error": 1}
    assert recommended_sensitivity_estimation(**kwargs) == 0.0
    # init kwargs
    kwargs: Dict[str, float] = {"signal": 10,
                                "background": 0,
                                "background_error": 0}
    assert recommended_sensitivity_estimation(**kwargs) == 0.0
    # init kwargs
    kwargs: Dict[str, float] = {"signal": 4,
                                "background": 2,
                                "background_error": 0}
    assert np.isclose(recommended_sensitivity_estimation(**kwargs), 2.27670)
    # init kwargs
    kwargs: Dict[str, float] = {"signal": 4,
                                "background": 2,
                                "background_error": 1}
    assert np.isclose(recommended_sensitivity_estimation(**kwargs), 1.722450)
