"""
Test the filtering functions in process_filters.py.
"""

import os
import sys
from typing import List

import numpy as np

# import matplotlib.pyplot as plt
from SATACO.processing_tools.process_filters import (apply_CR_filter,
                                                     apply_LH_expansion_filter,
                                                     apply_LH_reduction_filter,
                                                     apply_substring_filter)

# Append the project root directory to the Python path
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

# from SATACO.processing_tools.process_filters import apply_CR_filter

# DEPRECATED
# def test_apply_CR_filter():
#     path_matrix: str = os.path.join(os.path.dirname(__file__),
#                                     '..',
#                                     '..',
#                                     'tests_data',
#                                     'tests_data_processing_tools',
#                                     'upper_limit_matrix_CR_filter.txt'
#                                     )
#     # Read matrix from file.
#     matrix: np.ndarray = np.loadtxt(path_matrix,
#                                     skiprows=1,
#                                     dtype=float)

#     # Read column names from file.
#     with open(path_matrix, 'r') as f:
#         column_names: List[str] = f.readline().strip().split(" ")

#     threshold: float = 0.01

#     # Apply the CR filter.
#     matrix_CR_filter: np.ndarray = apply_CR_filter(matrix=matrix,
#                                                    column_names=column_names,
#                                                    threshold=threshold)
#     # Check if the matrices have the same shape.
#     assert matrix.shape == matrix_CR_filter.shape

#     # Check if the matrix is equal to the expected matrix
#     path_matrix_CR_filter_expected: str = os.path.join(os.path.dirname(__file__),
#                                                        '..',
#                                                        '..',
#                                                        'tests_data',
#                                                        'tests_data_processing_tools',
#                                                        'upper_limit_matrix_CR_filter_expected.txt')
#     # Read matrix from file.
#     matrix_CR_filter_expected: np.ndarray = np.loadtxt(path_matrix_CR_filter_expected,
#                                                        skiprows=1,
#                                                        dtype=float)

#     assert np.allclose(matrix_CR_filter,
#                        matrix_CR_filter_expected)


def test_apply_LH_expansion_filter():
    """
    Test for the expansion when using different likelihood scans.
    """
    # define symmetrical matrix
    matrix = np.array([[1., 0.2, 0., 0.],
                       [0.2, 1., 0., 0.3],
                       [0., 0., 1., 0.],
                       [0., 0.3, 0., 1.]])

    # define column names
    column_names = ["Ana0__CR0", "Ana0__SR0",
                    "Ana0__CR1", "Ana0__SR1"]

    # define LH column names
    LH_column_names = ["Ana0__LH0__CR0", "Ana0__LH0__SR0",
                       "Ana0__LH1__CR0", "Ana0__LH1__CR1", "Ana0__LH1__SR1", "Ana0__LH2__SR0"]

    # apply LH expansion filter
    matrix_expanded = apply_LH_expansion_filter(matrix=matrix,
                                                column_names=column_names,
                                                LH_column_names=LH_column_names)
    matrix_expanded_expected = np.array([[1., 0.2, 1., 0., 0., 0.2],
                                         [0.2, 1., 0.2, 0., 0.3, 1.],
                                         [1, 0.2, 1., 0., 0.0, 0.2],
                                         [0., 0., 0., 1., 0., 0.],
                                         [0., 0.3, 0., 0., 1., 0.3],
                                         [0.2, 1., 0.2, 0., 0.3, 1.]])
    assert np.allclose(matrix_expanded, matrix_expanded_expected)


def test_apply_LH_reduction_filter():
    """
    Test for the reduction when using different likelihood scans.
    """
    # define symmetrical matrix
    matrix = np.array([[1., 0.2, 0.2, 0., 0., 0.2],
                       [0.2, 1., 1., 0., 0.3, 1.],
                       [0.2, 1., 1., 0., 0.3, 1.],
                       [0., 0., 0., 1., 0., 0.],
                       [0., 0.3, 0.3, 0., 1., 0.3],
                       [0.2, 1., 1., 0., 0.3, 1.]])

    # define column names
    column_names = ["Ana0__CR0", "Ana0__SR0",
                    "Ana0__CR1", "Ana0__SR1"]

    # define LH column names
    LH_column_names = ["Ana0__LH0__CR0", "Ana0__LH0__SR0",
                       "Ana0__LH1__CR0", "Ana0__LH1__CR1", "Ana0__LH1__SR1", "Ana0__LH2__SR0"]

    # apply LH reduction filter
    matrix_reduced = apply_LH_reduction_filter(matrix=matrix,
                                               column_names=column_names,
                                               LH_column_names=LH_column_names)

    matrix_reduced_expected = np.array([[1., 0.2, 0., 0.],
                                        [0.2, 1., 0., 0.3],
                                        [0., 0., 1., 0.],
                                        [0., 0.3, 0., 1.]])
    assert np.allclose(matrix_reduced, matrix_reduced_expected)


def test_apply_substring_filter():
    """
    Test removing substring filter.
    """
    substring = ["CR0", "CR1"]

    column_names = ["SR0", "SR1", "CR0", "Analysis__CR1"]
    column_names_filterd = ["SR0", "SR1"]

    # matrix before filter
    matrix_with_CRs = np.array([[1., 0.2, 0., 0.],
                                [0.2, 1., 0., 0.3],
                                [0., 0., 1., 0.],
                                [0., 0.3, 0., 1.]])

    # true matrix after filter
    matrix_wo_CRs = np.array([[1., 0.2],
                              [0.2, 1.]])

    assert np.allclose(apply_substring_filter(matrix_with_CRs,
                                              column_names,
                                              substring)[0], matrix_wo_CRs)

    assert apply_substring_filter(matrix_with_CRs,
                                  column_names,
                                  substring)[1] == column_names_filterd


# if __name__ == "__main__":
#     # Plot the overlap matrix.
#     # load overlap matrix
#     path_overlap = os.path.join(os.path.dirname(__file__),
#                                 '..',
#                                 '..',
#                                 'tests_data',
#                                 'tests_data_processing_tools',
#                                 'upper_limit_matrix_CR_filter.txt'
#                                 )
#     # Read matrix from file.
#     upper_limit_matrix: np.ndarray = np.loadtxt(path_overlap,
#                                                 skiprows=1,
#                                                 dtype=float)

#     # Read column names from file.
#     with open(path_overlap, 'r') as f:
#         column_names: List[str] = f.readline().strip().split(" ")

#     # Plot upper limit matrix and CR matrix and upper limit matrix with applied CR filter
#     fig, axes = plt.subplots(1, 5, figsize=(30, 6))
#     axes[0].imshow(upper_limit_matrix, cmap='Blues',
#                    vmin=0.001, vmax=1)
#     axes[0].set_xticks(np.arange(len(column_names)))
#     axes[0].set_yticks(np.arange(len(column_names)))
#     axes[0].set_xticklabels(column_names, fontsize=4)
#     axes[0].set_yticklabels(column_names, fontsize=4)
#     axes[0].tick_params(top=True, bottom=False,
#                         labeltop=True, labelbottom=False)
#     plt.setp(axes[0].get_xticklabels(), rotation=90, ha="right")
#     axes[0].set_title("Upper Limit Matrix", fontsize=4)
#     # Set the colorbar on logarithmic scale
#     # cbar = fig.figure.colorbar(
#     #     axes[0].imshow(upper_limit_matrix, cmap='Blues', norm=LogNorm()))
#     # cbar.axes[0].set_ylabel("Upper Limit Value", rotation=-90, va="bottom")

#     # loop over all elements and add text
#     for i in range(len(column_names)):
#         for j in range(len(column_names)):
#             # add text to the plot
#             axes[0].text(j, i, upper_limit_matrix[i, j],
#                          ha="center", va="center", color="k", fontsize=4)

#     # add horizontal and vertical lines after the different likelihood scans
#     # different likelihood scans are defined as headers: <analysis>__<likelihood_scan>__<signal_region>

#     likelihood_scans = {}
#     for i, column_name in enumerate(column_names):
#         # split column name at '__'
#         split_column_name = column_name.split('__')

#         # check if the likelihood scan is already in the dictionary
#         if split_column_name[0]+'__'+split_column_name[1] not in likelihood_scans:
#             likelihood_scans[split_column_name[0]+'__' +
#                              split_column_name[1]] = {"region": [], "index": []}
#         likelihood_scans[split_column_name[0]+'__' +
#                          split_column_name[1]]["region"].append(split_column_name[2])
#         likelihood_scans[split_column_name[0] +
#                          '__'+split_column_name[1]]["index"].append(i)

#     # loop over all likelihood scans
#     for likelihood_scan in likelihood_scans:
#         # get the index of the likelihood scan
#         min_index = min(likelihood_scans[likelihood_scan]["index"])
#         max_index = max(likelihood_scans[likelihood_scan]["index"])
#         # add horizontal and vertical lines
#         axes[0].axhline(min_index-0.5, color='k')
#         axes[0].axhline(max_index+0.5, color='k')
#         axes[0].axvline(min_index-0.5, color='k')
#         axes[0].axvline(max_index+0.5, color='k')

#     headers_CR_indices = []
#     for idx, header in enumerate(column_names):
#         if "CR" in header:
#             headers_CR_indices.append(idx)

#     # Extract the CR matrix from the upper limit matrix.
#     CR_matrix = upper_limit_matrix[headers_CR_indices,
#                                    :][:, headers_CR_indices]

#     # Plot the CR matrix.
#     axes[1].imshow(CR_matrix, cmap='Blues',  vmin=0.001, vmax=1)
#     axes[1].set_xticks(np.arange(len(headers_CR_indices)))
#     axes[1].set_yticks(np.arange(len(headers_CR_indices)))
#     axes[1].set_xticklabels(np.array(column_names)[
#                             headers_CR_indices], fontsize=4)
#     axes[1].set_yticklabels(np.array(column_names)[
#                             headers_CR_indices], fontsize=4)
#     axes[1].tick_params(top=True, bottom=False,
#                         labeltop=True, labelbottom=False)
#     plt.setp(axes[1].get_xticklabels(), rotation=90, ha="right")
#     axes[1].set_title("CR Matrix", fontsize=4)

#     # loop over all elements and add text
#     for i in range(len(headers_CR_indices)):
#         for j in range(len(headers_CR_indices)):
#             # add text to the plot
#             axes[1].text(j, i, CR_matrix[i, j],
#                          ha="center", va="center", color="k", fontsize=4)

#     # add horizontal and vertical lines after the different likelihood scans
#     # different likelihood scans are defined as headers: <analysis>__<likelihood_scan>__<signal_region>
#     likelihood_scans_CR = {}
#     for i, column_name in enumerate(np.array(column_names)[headers_CR_indices]):
#         # split column name at '__'
#         split_column_name = column_name.split('__')

#         # check if the likelihood scan is already in the dictionary
#         if split_column_name[0]+'__'+split_column_name[1] not in likelihood_scans_CR:
#             likelihood_scans_CR[split_column_name[0]+'__' +
#                                 split_column_name[1]] = {"region": [], "index": []}
#         likelihood_scans_CR[split_column_name[0]+'__' +
#                             split_column_name[1]]["region"].append(split_column_name[2])
#         likelihood_scans_CR[split_column_name[0] +
#                             '__'+split_column_name[1]]["index"].append(i)

#     # loop over all likelihood scans
#     for likelihood_scan in likelihood_scans_CR:
#         # get the index of the likelihood scan
#         min_index = min(likelihood_scans_CR[likelihood_scan]["index"])
#         max_index = max(likelihood_scans_CR[likelihood_scan]["index"])
#         # add horizontal and vertical lines
#         axes[1].axhline(min_index-0.5, color='k')
#         axes[1].axhline(max_index+0.5, color='k')
#         axes[1].axvline(min_index-0.5, color='k')
#         axes[1].axvline(max_index+0.5, color='k')

#     # Apply the threshold to the CR matrix
#     threshold = 0.01
#     CR_matrix_threshold = (CR_matrix > threshold).astype(int)

#     # Check which indices are 1
#     indices = np.where(CR_matrix_threshold == 1)

#     combinations = []
#     combination_values = []
#     for i, j in zip(indices[0], indices[1]):
#         if i < j:
#             combinations.append((column_names[headers_CR_indices[i]],
#                                  column_names[headers_CR_indices[j]]))
#             combination_values.append(CR_matrix[i, j])

#     indices_combs = []
#     for combination in combinations:
#         lh_scan_0 = combination[0].split(
#             '__')[0]+'__'+combination[0].split('__')[1]

#         lh_scan_1 = combination[1].split(
#             '__')[0]+'__'+combination[1].split('__')[1]

#         indices_combs.append((likelihood_scans[lh_scan_0]["index"],
#                               likelihood_scans[lh_scan_1]["index"]))

#     upper_limit_matrix_CR_filter = upper_limit_matrix.copy()
#     # set all indices combs to combination value
#     for idx, comb in enumerate(indices_combs):
#         upper_limit_matrix_CR_filter[np.ix_(
#             comb[0], comb[1])] = combination_values[idx]
#         upper_limit_matrix_CR_filter[np.ix_(
#             comb[1], comb[0])] = combination_values[idx]

#     # Plot the upper limit matrix with applied CR filter.
#     axes[2].imshow(upper_limit_matrix_CR_filter, cmap='Blues',
#                    vmin=0.001, vmax=1)
#     axes[2].set_xticks(np.arange(len(column_names)))
#     axes[2].set_yticks(np.arange(len(column_names)))
#     axes[2].set_xticklabels(column_names, fontsize=4)
#     axes[2].set_yticklabels(column_names, fontsize=4)
#     axes[2].tick_params(top=True, bottom=False,
#                         labeltop=True, labelbottom=False)
#     plt.setp(axes[2].get_xticklabels(), rotation=90, ha="right")
#     axes[2].set_title("Upper Limit Matrix with CR Filter", fontsize=4)

#     # loop over all elements and add text
#     for i in range(len(column_names)):
#         for j in range(len(column_names)):
#             # add text to the plot
#             axes[2].text(j, i, upper_limit_matrix_CR_filter[i, j],
#                          ha="center", va="center", color="k", fontsize=4)

#     # add horizontal and vertical lines after the different likelihood scans

#     # loop over all likelihood scans
#     for likelihood_scan in likelihood_scans:
#         # get the index of the likelihood scan
#         min_index = min(likelihood_scans[likelihood_scan]["index"])
#         max_index = max(likelihood_scans[likelihood_scan]["index"])
#         # add horizontal and vertical lines
#         axes[2].axhline(min_index-0.5, color='k')
#         axes[2].axhline(max_index+0.5, color='k')
#         axes[2].axvline(min_index-0.5, color='k')
#         axes[2].axvline(max_index+0.5, color='k')

#     # plot upper limit matrix with applied threshold
#     axes[3].imshow(upper_limit_matrix > threshold, cmap='Blues')
#     axes[3].set_xticks(np.arange(len(column_names)))
#     axes[3].set_yticks(np.arange(len(column_names)))
#     axes[3].set_xticklabels(column_names, fontsize=4)
#     axes[3].set_yticklabels(column_names, fontsize=4)
#     axes[3].tick_params(top=True, bottom=False,
#                         labeltop=True, labelbottom=False)
#     plt.setp(axes[3].get_xticklabels(), rotation=90, ha="right")

#     # title
#     axes[3].set_title("Applied Threshold on Upper Limit Matrix", fontsize=4)

#     # plot applied filter upper limit matrix
#     axes[4].imshow(upper_limit_matrix_CR_filter > threshold, cmap='Blues')
#     axes[4].set_xticks(np.arange(len(column_names)))
#     axes[4].set_yticks(np.arange(len(column_names)))
#     axes[4].set_xticklabels(column_names, fontsize=4)
#     axes[4].set_yticklabels(column_names, fontsize=4)
#     axes[4].tick_params(top=True, bottom=False,
#                         labeltop=True, labelbottom=False)
#     plt.setp(axes[4].get_xticklabels(), rotation=90, ha="right")
#     # title
#     axes[4].set_title(
#         "Applied Threshold on Upper Limit Matrix with CR Filter", fontsize=4)

#     # tight layout

#     fig.tight_layout()
#     plt.show()
