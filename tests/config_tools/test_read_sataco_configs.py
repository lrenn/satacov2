"""
Testing the read_sataco_configs.py function.
"""

import pytest
from SATACO.config_tools.read_sataco_configs import read_sataco_configs
import sys
import os

# Append the project root directory to the Python path
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))


def test_read_sataco_configs():
    # specify the path of the test configuration files
    test_valid_config_file = os.path.join(
        os.path.dirname(__file__),
        '..',
        '..',
        'tests_data',
        'tests_data_config_tools',
        'test_sataco_valid.config')
    test_missing_option_config_file = os.path.join(
        os.path.dirname(__file__),
        'test_config_files',
        'test_sataco_missing_option.config')
    test_missing_section_config_file = os.path.join(
        os.path.dirname(__file__),
        '..',
        '..',
        'tests_data',
        'tests_data_config_tools',
        'test_sataco_missing_section.config')
    test_value_error_config_file = os.path.join(
        os.path.dirname(__file__),
        '..',
        '..',
        'tests_data',
        'tests_data_config_tools',
        'test_sataco_value_error.config')
    test_empty_config_file = os.path.join(
        os.path.dirname(__file__),
        '..',
        '..',
        'tests_data',
        'tests_data_config_tools',
        'test_sataco_empty.config')

    # test valid config file
    config_values = read_sataco_configs(test_valid_config_file)
    true_config_values = {
        'weight_method': 'recommended_sensitivity_estimation',
        'correlation_method': 'szymkiewicz_simpson',
        'confidence_level': 0.95,
        'correlation_threshold': 0.01,
        'combination_method': 'stouffer',
        'top_combinations': 1
    }
    assert config_values == true_config_values

    # test missing option config file
    with pytest.raises(SystemExit):
        config_values = read_sataco_configs(test_missing_option_config_file)

    # test missing section config file
    with pytest.raises(SystemExit):
        config_values = read_sataco_configs(test_missing_section_config_file)

    # test value error config file
    with pytest.raises(SystemExit):
        config_values = read_sataco_configs(test_value_error_config_file)

    # test non-existent config file
    with pytest.raises(SystemExit):
        config_values = read_sataco_configs('non-existent.config')

    # test empty config file
    with pytest.raises(SystemExit):
        config_values = read_sataco_configs(test_empty_config_file)
