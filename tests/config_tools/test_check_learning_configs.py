"""
Test the functions that check the type and
value of the config file arguments.
"""

from SATACO.config_tools.check_learning_configs import __check_acquisition_function as check_acquisition_function
from SATACO.config_tools.check_learning_configs import __check_minimization_function as check_minimization_function
from SATACO.config_tools.check_learning_configs import __check_acquisition_optimizer as check_acquisition_optimizer
from SATACO.config_tools.check_learning_configs import __check_pmssm_parameters as check_pmssm_parameters
from SATACO.config_tools.check_learning_configs import __check_signal_eff_function as check_signal_eff_function
from SATACO.config_tools.check_learning_configs import __check_signal_eff_alpha as check_signal_eff_alpha
from SATACO.config_tools.check_learning_configs import __check_num_next_points as check_num_next_points
import sys
import os

# Append the project root directory to the Python path
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))


def test_check_acquisition_function():
    """
    Test the check_acquisition_function function.
    """
    # test if the function returns True for a 'EI'
    assert check_acquisition_function('EI') == True

    # test if the function returns True for a 'PI'
    assert check_acquisition_function('PI') == True

    # test if the function returns True for a 'UCB'
    assert check_acquisition_function('LCB') == True

    # test if the function returns False for a wrong type
    assert check_acquisition_function(1) == False

    # test if the function returns False for a wrong value
    assert check_acquisition_function('not_valid') == False


def test_check_minimization_function():
    """
    Test the check_minimization_function function.
    """
    # test if the function returns True for a 'DUMMY'
    assert check_minimization_function('DUMMY') == True

    # test if the function returns True for a 'FOREST'
    assert check_minimization_function('FOREST') == True

    # test if the function returns True for a 'GP'
    assert check_minimization_function("GP") == True

    # test if the function returns True for a 'GBRT'
    assert check_minimization_function('GBRT') == True

    # test if the function returns False for a wrong type
    assert check_minimization_function(1) == False

    # test if the function returns False for a wrong value
    assert check_minimization_function('not_valid') == False


def test_check_acquisition_optimizer():
    """
    Test the check_acquisition_optimizer function.
    """
    # test if the function returns True for a 'SAMPLING'
    assert check_acquisition_optimizer('SAMPLING') == True

    # test if the function returns True for a 'LBFGS'
    assert check_acquisition_optimizer('LBFGS') == True

    # test if the function returns False for a wrong type
    assert check_acquisition_optimizer(1) == False

    # test if the function returns False for a wrong value
    assert check_acquisition_optimizer('not_valid') == False


def test_check_pmssm_parameters():
    """
    Test the check_pmssm_parameters function.
    """
    # test if the function returns True for 'ALL'
    assert check_pmssm_parameters('ALL') == True

    # test if the function returns True for single parameters
    single_parameters = [
        'tanb',
        'M_1',
        'M_2',
        'M_3',
        'At',
        'Ab',
        'Atau',
        'mu',
        'mA',
        'meL',
        'mmuL',
        'mtauL',
        'meR',
        'mmuR',
        'mtauR',
        'mqL1',
        'mqL2',
        'mqL3',
        'muR',
        'mcR',
        'mtR',
        'mdR',
        'msR',
        'mbR'
    ]
    for parameter in single_parameters:
        assert check_pmssm_parameters(parameter) == True

    # test if the function returns True for a comma seperated list of parameters
    comma_seperated_list_all = 'tanb, M_1, M_2, M_3, At, Ab, Atau, mu, mA, meL, mmuL, mtauL, meR, mmuR, mtauR, mqL1, mqL2, mqL3, muR, mcR, mtR, mdR, msR, mbR'
    assert check_pmssm_parameters(comma_seperated_list_all) == True

    # test if the function returns True for a comma seperated list of parameters
    comma_seperated_list_some = 'M_1, M_2, M_3, mu, tanb'
    assert check_pmssm_parameters(comma_seperated_list_some) == True

    # test if the function returns False for an empty string
    assert check_pmssm_parameters(',') == False

    # test if the function returns False for duplicate parameters
    assert check_pmssm_parameters('M_1, M_1') == False

    # test if the function returns False for a wrong type
    assert check_pmssm_parameters(1) == False

    # test if the function returns False for a wrong value
    assert check_pmssm_parameters('not_valid') == False

    # test if the function returns False for a wrong value
    assert check_pmssm_parameters('M_1, not_valid') == False


def test_check_signal_eff_function():
    """
    Test the check_signal_eff_function function.
    """
    # test if the function returns True for a 'ONE'
    assert check_signal_eff_function('ONE') == True

    # test if the function returns True for a 'TWO'
    assert check_signal_eff_function('TWO') == True

    # test if the function returns False for a wrong type
    assert check_signal_eff_function(1) == False

    # test if the function returns False for a wrong value
    assert check_signal_eff_function('not_valid') == False


def test_check_signal_eff_alpha():
    """
    Test the check_signal_eff_alpha function.
    """
    # test if the function returns True for a 0.95
    assert check_signal_eff_alpha(0.95) == True

    # test if the function returns True for a 0.99
    assert check_signal_eff_alpha(0.99) == True

    # test if the function returns False for -0.01
    assert check_signal_eff_alpha(-0.01) == False

    # test if the function returns False for 1.01
    assert check_signal_eff_alpha(1.01) == False

    # test if the function returns False for a wrong type
    assert check_signal_eff_alpha('not_valid') == False


def test_check_num_next_points():
    """
    Test the check_num_next_points function.
    """
    # test if the function returns True for a 1
    assert check_num_next_points(1) == True

    # test if the function returns True for a 2
    assert check_num_next_points(2) == True

    # test if the function returns False for a 0
    assert check_num_next_points(0) == False

    # test if the function returns False for a -1
    assert check_num_next_points(-1) == False

    # test if the function returns False for a wrong type
    assert check_num_next_points('not_valid') == False
