"""
Testing functions for the summary files.
"""

import sys
import os
from SATACO.summary_tools.summary_files import append_timestamp_to_filename
from datetime import datetime


# Append the project root directory to the Python path
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))


def test_append_timestamp_to_filename():
    """
    Test the function append_timestamp_to_filename.
    """
    # test empty string
    assert append_timestamp_to_filename("") == ""

    # test non-empty string
    assert append_timestamp_to_filename(
        "test.txt") == f"test_{datetime.now().strftime('%Y%m%d%H%M')}.txt"
