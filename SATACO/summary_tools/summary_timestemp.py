"""
Appending timestemps on file paths.
"""

import os
from datetime import datetime

def append_timestamp_to_filename(file_path: str) -> str:
    """
    Append a timestamp to the filename of a file path.

    Args:
        file_path (str): the path to the file

    Returns:
        (str): the path to the file with the timestamp appended to the filename.
    """

    if file_path == "":
        return file_path
    # Split the path into directory and filename
    directory, filename = os.path.split(file_path)

    # Split the filename into name and extension
    name, extension = os.path.splitext(filename)

    # Get the current timestamp in a suitable format
    timestamp = datetime.now().strftime("%Y%m%d%H%M")

    # Append the timestamp to the filename and reconstruct the path
    new_filename = f"{name}_{timestamp}{extension}"
    new_file_path = os.path.join(directory, new_filename)

    return new_file_path

