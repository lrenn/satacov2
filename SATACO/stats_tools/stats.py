"""
Calculating the statistics of the correlation matrix for different
correlation functions.
"""


from typing import Tuple

import numpy as np
from scipy.stats import beta


def calculate_statistics_of_correlations(shared_events: np.ndarray,
                                         threshold: float,
                                         confidence_level: float = 0.95,
                                         correlation_method: str = 'jaccard') -> Tuple[np.ndarray, np.ndarray]:
    """
    Calculate the statistics of the correlations for different correlation
    methods.

    Args:
        shared_events (np.ndarray): Matrix of combinations of signal regions/analyses
            with number of events accepted by both.
        threshold (float): Threshold for the probability of events to be accepted by both
            when accepting them by either one of them.
        confidence_level (float, optional): Significance level for the binomial test.
            Defaults to 0.95.
        correlation_method (str, optional): Method to calculate the correlation. Defaults to 'jaccard'.
            Also possible: 'geometrical', 'pearson', 'sorensen_dice', 'otsuka_ochiai', 'cosine', 'szymkiewicz_simpson'.  

    Returns:
        Tuple[np.ndarray, np.ndarray, np.ndarray]: Matrix with boolean values indicating whether the correlation
            is significant or not and the p-values for the binomial test.
    """
    # initialize p-values and significant correlations
    upper_limit = np.zeros(shared_events.shape).astype(float)
    signficant_correlations = np.ones(shared_events.shape).astype(
        int)  # by default all correlations are significant

    # check which correlation method is used
    if correlation_method == 'geometrical' or correlation_method == 'jaccard':
        # binomial test for geometrical/jaccard correlation
        # H_0: p =< threshold (p = probability of events to be accepted by both when accepting them by either one of them)
        # H_1: p > threshold (p = probability of events to be accepted by both when accepting them by either one of them)
        # significance level
        # alternative: 'greater' (p > threshold)

        # calculate p-value for each combination

        # Calculate accepted events by either one of the analyses
        accepted_events_either = (np.ones(
            shared_events.shape) * np.diag(shared_events)).T + np.ones(shared_events.shape) * np.diag(shared_events) - shared_events

        # calculate p-value
        upper_limit = beta.ppf(confidence_level,
                               shared_events + 1,
                               accepted_events_either - shared_events + 1)

        # assign values to significant correlations (-1: not enough data, 0: significantly not correlated, 1: significantly correlated)

        # not enough data
        # case a: no events accepted by either one of the analyses
        signficant_correlations[accepted_events_either == 0] = -1

        # case b: quotient of shared events and accepted events is lower than threshold but upper p-value is greater than threshold
        signficant_correlations[((shared_events/accepted_events_either) < threshold) &
                                (upper_limit > threshold)] = -1

        # significantly not correlated
        # quotient of shared events and accepted events is lower than threshold and upper p-value is lower than threshold
        signficant_correlations[(upper_limit <= threshold)] = 0

        # significantly correlated
        # all others will be marked as significantly correlated
        # Marked by default

    elif correlation_method == 'szymkiewicz_simpson':
        # get the minimum of accepted events in either one of the analyses for each combination
        accepted_events_either_min = np.minimum((np.ones(shared_events.shape) * np.diag(shared_events)).T, np.ones(
            shared_events.shape) * np.diag(shared_events))  # np.minimum is element-wise

        # calculate p-value
        upper_limit = beta.ppf(confidence_level,
                               shared_events + 1,
                               accepted_events_either_min - shared_events + 1)

        # assign values to significant correlations (-1: not enough data, 0: significantly not correlated, 1: significantly correlated)

        # not enough data
        # case a: no events accepted by either one of the analyses
        signficant_correlations[accepted_events_either_min == 0] = -1

        # case b: quotient of shared events and accepted events is lower than threshold but upper p-value is greater than threshold
        signficant_correlations[(shared_events/accepted_events_either_min < threshold) &
                                (upper_limit > threshold)] = -1

        # significantly not correlated
        # quotient of shared events and accepted events is lower than threshold and upper p-value is lower than threshold
        signficant_correlations[(upper_limit <= threshold)] = 0

        # significantly correlated
        # all others will be marked as significantly correlated
        # Marked by default

    else:
        raise NotImplementedError(
            'Only the geometrical/jaccard and the szymkiewicz_simpson correlation are implemented.')

    # make diagonal entries of significant correlations 1
    np.fill_diagonal(signficant_correlations, 1)

    # make diagonal entries of p-values 1
    np.fill_diagonal(upper_limit, 1)

    return signficant_correlations, upper_limit


def reassigning_sufficient_statistics(correlation_matrix: np.ndarray,
                                      upper_limit_matrix: np.ndarray,
                                      threshold: float = 0.01) -> np.ndarray:
    """
    Reassign the sufficient statistics after the control region filtering.

    Args:
        correlation_matrix (np.ndarray): Correlation matrix.
        upper_limit_matrix (np.ndarray): Upper limit matrix.
        threshold (float, optional): Threshold on both matrices. Defaults to 0.01.

    Returns:
        np.ndarray: Recalculated sufficient statistics matrix.
    """
    sufficient_stats_matrix = np.ones_like(correlation_matrix)
    sufficient_stats_matrix[np.isnan(correlation_matrix)] = -1
    sufficient_stats_matrix[(correlation_matrix < threshold) &
                            (upper_limit_matrix > threshold)] = -1
    sufficient_stats_matrix[(upper_limit_matrix <= threshold)] = 0

    return sufficient_stats_matrix
