"""
File containing functions to plot matrices (e.g. correlation/overlap matrix,
events matrix, etc.)
"""
import os
from typing import Any, Dict, List, Union

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from matplotlib.colors import LinearSegmentedColormap, LogNorm
from matplotlib.patches import Rectangle

from SATACO.plotting_tools.plot_style import set_plot_style
from SATACO.plotting_tools.utils import (__check_variables_validity,
                                         __extract_analysis_indices,
                                         __extract_likelihood_scan_indices,
                                         __pretty_event_number)
from SATACO.processing_tools.process_overlaps import (
    jaccard_index, szymkiewicz_simpson_index)
from SATACO.processing_tools.process_sorting import sort_matrix_column_names
from SATACO.stats_tools.stats import calculate_statistics_of_correlations


def plot_transition_matrix(df: pd.DataFrame,
                           xvar: str,
                           yvar: str,
                           bin_edges: np.ndarray = np.array(
                               [0., 0.05, 0.35, 1.]),
                           norm: bool = False,
                           save: str = "") -> None:
    """
    Plot the transition matrix for a given dataframe to compare the performance
    of the SATACO algorithm to the normal likelihoods.

    Args:
        df (pd.DataFrame): Dataframe containing the data to plot.
        xvar (str): Name of the x-axis variable.
        yvar (str): Name of the y-axis variable.
        bin_edges (np.ndarray): Bin edges for the transition matrix. Defaults to np.array([0., 0.05, 0.35, 1.]).

    Returns:
        None
    """
    # check if xvar and yvar are in the dataframe
    __check_variables_validity(df=df, xvar=xvar, yvar=yvar)

    # categorize xvar and yvar dependent in which interval they are
    # intervals start at 0 and end at 1
    # add categorical variables to dataframe
    df["xvar_cat"] = pd.cut(df[xvar], bins=bin_edges,
                            labels=False, include_lowest=True)
    df["yvar_cat"] = pd.cut(df[yvar], bins=bin_edges,
                            labels=False, include_lowest=True)

    transition_matrix: np.ndarray = np.zeros(
        (bin_edges.shape[0] - 1, bin_edges.shape[0] - 1))

    for idx in range(bin_edges.shape[0] - 1):
        for idy in range(bin_edges.shape[0] - 1):
            # get the number of events in the interval
            n_events = len(df[(df["xvar_cat"] == idx)
                           & (df["yvar_cat"] == idy)])
            transition_matrix[idx, idy] = n_events

    # normalize transition matrix
    if norm:
        transition_matrix = transition_matrix / \
            transition_matrix.sum(axis=1, keepdims=True)

    # set ATLAS style
    set_plot_style(matrix=True)
    fig, ax = plt.subplots(figsize=(11, 10))

    # plot matrix
    cbar = ax.figure.colorbar(
        ax.imshow(transition_matrix, cmap='Greys', vmin=0), ax=ax)
    cbar.ax.tick_params(labelsize=10)
    cbar_label: str = "Number of model points"
    if norm:
        cbar_label = "Normalized number of model points"
    cbar.ax.set_ylabel(cbar_label, fontdict={
        "family": "sans-serif", "fontsize": 18, })

    if not norm:
        # add the number of events in each bin as text
        for i in range(transition_matrix.shape[0]):
            for j in range(transition_matrix.shape[1]):
                ax.text(j, i, int(transition_matrix[i, j]),
                        ha="center", va="center", color="r")

    # Make labels centered
    ax.xaxis.set_ticks_position("top")
    ax.xaxis.set_label_position("top")
    # Set the labels
    ax.set_xticks(np.arange(len(bin_edges) - 1), minor=False)
    ax.set_yticks(np.arange(len(bin_edges) - 1), minor=False)
    labels = [r"$<$ " + str(round(bin_edge, 3)) for bin_edge in bin_edges[1:]]
    ax.set_xticklabels(labels, fontfamily="sans-serif")
    ax.set_yticklabels(labels, fontfamily="sans-serif")

    # tight layout
    fig.tight_layout()

    # save figure
    if save != "":
        plt.savefig(save)
        plt.close()
    else:
        plt.show()

    return


def plot_overlap_matrix(matrix: np.ndarray,
                        column_names: List[str] = [],
                        save: str = "",
                        events: str = "",
                        lognorm: bool = False,
                        mark_likelihood_scans: bool = False,
                        production_mode_grouping: Dict[str, str] = {},
                        mark_nan_values: bool = True,
                        no_labels: bool = False) -> None:
    """
    Plot overlap matrix.

    Args:
        matrix (np.ndarray): Matrix with overlap values.
        column_names(List[str]): List of column names. Defaults to [].
        save (str): Path to save plot.
        events (str): Number of events.
        lognorm (bool): Logarithmic normalization of the colorbar. Defaults to False.
        mark_likelihood_scans (bool): Mark likelihood scans in the matrix (vertical and horizontal lines). 
            Defaults to False.
        mark_nan_values (bool): Mark nan values in the matrix. Defaults to True.
        production_mode_grouping (Dict[str, str]): Dictionary with production mode grouping. Defaults to {}.
        no_labels (bool): Do not show labels. Defaults to False.

    Returns:
        None
    """
    # when production mode grouping is given, group the columns (1. EW, 2. Strong, 3. Other)
    if production_mode_grouping:
        EW_column_names: List[str] = []
        Strong_column_names: List[str] = []
        Other_column_names: List[str] = []
        # itertae over all column names
        for column_name in column_names:
            analysis_name = column_name.split("__")[0].strip()
            if production_mode_grouping[analysis_name] == "EW":
                EW_column_names.append(column_name)
            elif production_mode_grouping[analysis_name] == "Strong":
                Strong_column_names.append(column_name)
            else:
                Other_column_names.append(column_name)

        # sort all column names alphabetically
        EW_column_names.sort()
        Strong_column_names.sort()
        Other_column_names.sort()

        # assign indices
        production_indices: List[int] = [len(EW_column_names),
                                         len(EW_column_names) + len(Strong_column_names)]

        # concatenate all column names
        column_names_new = EW_column_names + Strong_column_names + Other_column_names

        # sort the matrix
        matrix = sort_matrix_column_names(matrix=matrix,
                                          column_names_original=column_names,
                                          column_names_new=column_names_new)

        # set column names
        column_names = column_names_new

    # set ATLAS style
    set_plot_style(matrix=True)

    # define figure
    fig, ax = plt.subplots(figsize=(11, 10))

    # plot matrix
    if lognorm:
        cbar = ax.figure.colorbar(
            ax.imshow(matrix, cmap='Greys', norm=LogNorm(vmin=1e-7, vmax=1)), ax=ax)
    else:
        cbar = ax.figure.colorbar(
            ax.imshow(matrix, cmap='Greys', vmin=0, vmax=1), ax=ax)
    cbar.ax.tick_params(labelsize=10)
    cbar.ax.set_ylabel("Overlap", fontdict={
                       "family": "sans-serif", "fontsize": 18, })

    # define column names if not given
    if column_names == []:
        column_names = [str(i) for i in range(matrix.shape[0])]

    # Make labels centered
    ax.xaxis.set_ticks_position("top")
    ax.xaxis.set_label_position("top")
    # Set the labels
    ax.set_xticks(np.arange(len(column_names)), minor=False)
    ax.set_yticks(np.arange(len(column_names)), minor=False)
    ax.set_xticklabels(column_names, fontfamily="sans-serif")
    ax.set_yticklabels(column_names, fontfamily="sans-serif")
    # Rotate the x labels
    plt.setp(ax.get_xticklabels(), rotation=90,
             ha="left", rotation_mode="anchor")

    # if number of events is given, add it as textbox in right corner
    if events != "":
        ax.text(0.95, 0.05, f"n = {__pretty_event_number(event_number=events)}", transform=ax.transAxes,
                fontsize=5, verticalalignment='bottom',
                horizontalalignment='right', bbox=dict(facecolor='white', alpha=0.5))

    # loop over signal regions and add horizontal and vertical lines
    border_color = "k"
    for i in range(matrix.shape[0]):
        ax.axhline(i - 0.5, color=border_color, linewidth=0.5)
        ax.axvline(i - 0.5, color=border_color, linewidth=0.5)

    # if mark_likelihood_scans is True, mark likelihood scans in the matrix (vertical and horizontal lines)
    if mark_likelihood_scans:
        likelihood_scans: Dict[str, Dict[str, List[Any]]] = __extract_likelihood_scan_indices(
            column_names=column_names)

        # mark analysis indices
        analyses: Dict[str, List[Any]] = __extract_analysis_indices(
            column_names=column_names)

        # loop over all likelihood scans
        for likelihood_scan in likelihood_scans:
            # get the index of the likelihood scan
            min_index = min(likelihood_scans[likelihood_scan]["index"])
            max_index = max(likelihood_scans[likelihood_scan]["index"])
            # add horizontal and vertical lines
            ax.axhline(min_index - 0.5, color=border_color, linewidth=2)
            ax.axhline(max_index + 0.5, color=border_color, linewidth=2)
            ax.axvline(min_index - 0.5, color=border_color, linewidth=2)
            ax.axvline(max_index + 0.5, color=border_color, linewidth=2)

        for analysis in analyses:
            # get the index of the analysis
            min_index = min(analyses[analysis])
            max_index = max(analyses[analysis])
            # add horizontal and vertical lines
            ax.axhline(min_index - 0.5, color=border_color, linewidth=4)
            ax.axhline(max_index + 0.5, color=border_color, linewidth=4)
            ax.axvline(min_index - 0.5, color=border_color, linewidth=4)
            ax.axvline(max_index + 0.5, color=border_color, linewidth=4)

    # if groupings are given, add horizontal and vertical lines (purple)
    if production_mode_grouping:
        # loop over all indices
        for production_index in production_indices:
            # add horizontal and vertical lines
            ax.axhline(production_index - 0.5, color=border_color, linewidth=8)
            ax.axvline(production_index - 0.5, color=border_color, linewidth=8)

    # if no_labels is True, remove labels
    if no_labels:
        ax.set_xticklabels([])
        ax.set_yticklabels([])

    if mark_nan_values:
        s = {10: 1000, 100: 50, 50: 5}
        # marker depednent on the size of the matrix
        for size, marker in s.items():
            if matrix.shape[0] >= size:
                break
        # Mark the nan values
        for i in range(len(column_names)):
            for j in range(len(column_names)):
                if np.isnan(matrix[i, j]):
                    ax.scatter(j, i, marker="x", color="red",
                               s=marker)

    # tight layout
    fig.tight_layout()

    # save figure
    if save != "":
        plt.savefig(save)
        plt.close()
    else:
        plt.show()

    return


def plot_events_matrices(shared_events: np.ndarray,
                         column_names: List[str] = [],
                         save: str = "",
                         events: str = "",
                         lognorm: bool = False,
                         mark_likelihood_scans: bool = False,
                         production_mode_grouping: Dict[str, str] = {},
                         no_labels: bool = False) -> None:
    """
    Plot shared events and accepted events matrix next to each other.

    Args:
        shared_events (np.ndarray): Matrix with shared events.
        accepted_events_either (np.ndarray): Matrix with accepted events.
        column_names(List[str]): List of column names. Defaults to [].
        save (str): Path to save plot.
        events (str): Number of events.
        lognorm (bool): Logarithmic normalization of the colorbar. Defaults to False.
        mark_likelihood_scans (bool): Mark likelihood scans in the matrix (vertical and horizontal lines).
        production_mode_grouping (Dict[str, str]): Dictionary with production mode grouping. Defaults to {}.
        no_labels (bool): Do not show labels. Defaults to False.

    Returns:
        None
    """
    # when production mode grouping is given, group the columns (1. EW, 2. Strong, 3. Other)
    if production_mode_grouping != {}:
        EW_column_names: List[str] = []
        Strong_column_names: List[str] = []
        Other_column_names: List[str] = []
        # itertae over all column names
        for column_name in column_names:
            analysis_name = column_name.split("__")[0].strip()
            if production_mode_grouping[analysis_name] == "EW":
                EW_column_names.append(column_name)
            elif production_mode_grouping[analysis_name] == "Strong":
                Strong_column_names.append(column_name)
            else:
                Other_column_names.append(column_name)

        # sort all column names alphabetically
        EW_column_names.sort()
        Strong_column_names.sort()
        Other_column_names.sort()

        # assign indices
        production_indices: List[int] = [len(EW_column_names),
                                         len(EW_column_names) + len(Strong_column_names)]

        # concatenate all column names
        column_names_new = EW_column_names + Strong_column_names + Other_column_names

        # sort the matrix
        shared_events = sort_matrix_column_names(matrix=shared_events,
                                                 column_names_original=column_names,
                                                 column_names_new=column_names_new)

        # set column names
        column_names = column_names_new
    # define figure
    fig, ax = plt.subplots(figsize=(11, 10))

    # set ATLAS style
    set_plot_style(matrix=True)

    if lognorm:
        cbar = ax.figure.colorbar(
            ax.imshow(shared_events, cmap='Greys', norm=LogNorm()), ax=ax)
    else:
        cbar = ax.figure.colorbar(
            ax.imshow(shared_events, cmap='Greys', vmin=0), ax=ax)
    cbar.ax.tick_params(labelsize=10)
    cbar.ax.set_ylabel(r"$N$", fontdict={
                       "family": "sans-serif", "fontsize": 18, })

    # define column names if not given
    if column_names == []:
        column_names = [str(i) for i in range(shared_events.shape[0])]

    # Make labels centered
    ax.xaxis.set_ticks_position("top")
    ax.xaxis.set_label_position("top")
    # Set the labels
    ax.set_xticks(np.arange(len(column_names)), minor=False)
    ax.set_yticks(np.arange(len(column_names)), minor=False)
    ax.set_xticklabels(column_names, fontfamily="sans-serif")
    ax.set_yticklabels(column_names, fontfamily="sans-serif")
    # Rotate the x labels
    plt.setp(ax.get_xticklabels(), rotation=90,
             ha="left", rotation_mode="anchor")

    # loop over signal regions and add horizontal and vertical lines
    border_color = "k"
    for i in range(shared_events.shape[0]):
        ax.axhline(i - 0.5, color=border_color, linewidth=0.5)
        ax.axvline(i - 0.5, color=border_color, linewidth=0.5)

    # if mark_likelihood_scans is True, mark likelihood scans in the matrix (vertical and horizontal lines)
    if mark_likelihood_scans:
        likelihood_scans: Dict[str, Dict[str, List[Any]]] = __extract_likelihood_scan_indices(
            column_names=column_names)

        # mark analysis indices
        analyses: Dict[str, List[Any]] = __extract_analysis_indices(
            column_names=column_names)

        # loop over all likelihood scans
        for likelihood_scan in likelihood_scans:
            # get the index of the likelihood scan
            min_index = min(likelihood_scans[likelihood_scan]["index"])
            max_index = max(likelihood_scans[likelihood_scan]["index"])
            # add horizontal and vertical lines
            ax.axhline(min_index - 0.5, color=border_color, linewidth=2)
            ax.axhline(max_index + 0.5, color=border_color, linewidth=2)
            ax.axvline(min_index - 0.5, color=border_color, linewidth=2)
            ax.axvline(max_index + 0.5, color=border_color, linewidth=2)

        for analysis in analyses:
            # get the index of the analysis
            min_index = min(analyses[analysis])
            max_index = max(analyses[analysis])
            # add horizontal and vertical lines
            ax.axhline(min_index - 0.5, color=border_color, linewidth=4)
            ax.axhline(max_index + 0.5, color=border_color, linewidth=4)
            ax.axvline(min_index - 0.5, color=border_color, linewidth=4)
            ax.axvline(max_index + 0.5, color=border_color, linewidth=4)

    # if groupings are given, add horizontal and vertical lines (purple)
    if production_mode_grouping:
        # loop over all indices
        for production_index in production_indices:
            # add horizontal and vertical lines
            ax.axhline(production_index - 0.5, color=border_color, linewidth=8)
            ax.axvline(production_index - 0.5, color=border_color, linewidth=8)

    # if no_labels is True, remove labels
    if no_labels:
        ax.set_xticklabels([])
        ax.set_yticklabels([])

    # tight layout
    fig.tight_layout()

    # save figure
    if save != "":
        plt.savefig(save)
        plt.close()
    else:
        plt.show()

    return


def plot_statistics_matrices(significant_correlations: np.ndarray,
                             upper_limits: np.ndarray,
                             column_names: List[str] = [],
                             save: str = "",
                             events: str = "",
                             lognorm: bool = False,
                             mark_likelihood_scans: bool = False,
                             mark_nan_values: bool = True,
                             production_mode_grouping: Dict[str, str] = {},
                             no_labels: bool = False) -> None:
    """
    Plot significant correlations and upper limit matrix next to each other.

    Args:
        significant_correlations (np.ndarray): Matrix with significant correlations.
        upper_limits (np.ndarray): Matrix with upper limits.
        column_names(List[str]): List of column names. Defaults to [].
        save (str): Path to save plot.
        events (str): Number of events.
        lognorm (bool): Logarithmic normalization of the colorbar. Defaults to False.
        mark_likelihood_scans (bool): Mark likelihood scans in the matrix (vertical and horizontal lines).
        production_mode_grouping (Dict[str, str]): Dictionary with production mode grouping. Defaults to {}.
        no_labels (bool): Do not show labels. Defaults to False.

    Returns:
        None
    """
    # when production mode grouping is given, group the columns (1. EW, 2. Strong, 3. Other)
    if production_mode_grouping != {}:
        EW_column_names: List[str] = []
        Strong_column_names: List[str] = []
        Other_column_names: List[str] = []
        # itertae over all column names
        for column_name in column_names:
            analysis_name = column_name.split("__")[0].strip()
            if production_mode_grouping[analysis_name] == "EW":
                EW_column_names.append(column_name)
            elif production_mode_grouping[analysis_name] == "Strong":
                Strong_column_names.append(column_name)
            else:
                Other_column_names.append(column_name)

        # sort all column names alphabetically
        EW_column_names.sort()
        Strong_column_names.sort()
        Other_column_names.sort()

        # assign indices
        production_indices: List[int] = [len(EW_column_names),
                                         len(EW_column_names) + len(Strong_column_names)]

        # concatenate all column names
        column_names_new = EW_column_names + Strong_column_names + Other_column_names

        # sort the matrix
        significant_correlations = sort_matrix_column_names(matrix=significant_correlations,
                                                            column_names_original=column_names,
                                                            column_names_new=column_names_new)
        upper_limits = sort_matrix_column_names(matrix=upper_limits,
                                                column_names_original=column_names,
                                                column_names_new=column_names_new)

        # set column names
        column_names = column_names_new
    # check if matrices have the same shape
    if significant_correlations.shape != upper_limits.shape:
        raise ValueError(
            "Matrices have different shapes: {} and {}".format(significant_correlations.shape, upper_limits.shape))

    # define figure
    fig, ax = plt.subplots(1, 2, figsize=(20, 10))

    # set ATLAS style
    set_plot_style(matrix=True)

    # plot matrix significant correlations (-1: Not enough data, 0: Not Overlapping (<1%), 1: Overlapping (>=1%))
    # assign red color to -1, green to 0 and blue to 1
    # create colormap only with green
    cmap = LinearSegmentedColormap.from_list(
        "custom_cmap", ["red", "green", "blue"], N=256)
    ax[0].imshow(significant_correlations, cmap=cmap, vmin=-1, vmax=1)

    # plot matrix upper limits
    cmap1 = plt.cm.get_cmap('Greys')
    # check if any nan values are present and replace them with -1
    if np.isnan(upper_limits).any():
        upper_limits = np.where(
            np.isnan(upper_limits), -1., upper_limits)

    if lognorm:
        cbar = ax[1].figure.colorbar(ax[1].imshow(
            upper_limits, cmap=cmap1, norm=LogNorm(vmin=1e-7, vmax=1)), ax=ax[1])
    else:
        cbar = ax[1].figure.colorbar(ax[1].imshow(
            upper_limits, cmap=cmap1, vmin=0, vmax=1), ax=ax[1])
    cbar.ax.tick_params(labelsize=10)
    cbar.ax.set_ylabel(r"Upper limit at 95\%", fontdict={
        "family": "sans-serif", "fontsize": 18, })

    # if number of events is given, add it as textbox in right corner
    if events != "":
        ax[0].text(0.95, 0.05, f"n = {__pretty_event_number(event_number=events)}", transform=ax[0].transAxes,
                   fontsize=5, verticalalignment='bottom',
                   horizontalalignment='right', bbox=dict(facecolor='white', alpha=0.5))
        ax[1].text(0.95, 0.05, f"n = {__pretty_event_number(event_number=events)}", transform=ax[1].transAxes,
                   fontsize=5, verticalalignment='bottom',
                   horizontalalignment='right', bbox=dict(facecolor='white', alpha=0.5))

    # define column names if not given
    if column_names == []:
        column_names = [str(i)
                        for i in range(significant_correlations.shape[0])]

    # Make labels centered
    ax[0].xaxis.set_ticks_position("top")
    ax[0].xaxis.set_label_position("top")
    ax[1].xaxis.set_ticks_position("top")
    ax[1].xaxis.set_label_position("top")
    # Set the labels
    ax[0].set_xticks(np.arange(len(column_names)), minor=False)
    ax[0].set_yticks(np.arange(len(column_names)), minor=False)
    ax[0].set_xticklabels(column_names, fontfamily="sans-serif")
    ax[0].set_yticklabels(column_names, fontfamily="sans-serif")
    ax[1].set_xticks(np.arange(len(column_names)), minor=False)
    ax[1].set_yticks(np.arange(len(column_names)), minor=False)
    ax[1].set_xticklabels(column_names, fontfamily="sans-serif")
    ax[1].set_yticklabels(column_names, fontfamily="sans-serif")
    # Rotate the x labels
    plt.setp(ax[0].get_xticklabels(), rotation=90,
             ha="left", rotation_mode="anchor")
    plt.setp(ax[1].get_xticklabels(), rotation=90,
             ha="left", rotation_mode="anchor")

    # set legend for significant correlations plot
    # -1: Not enough data, 0: Not Overlapping (<1%), 1: Overlapping (>=1%)
    # assign red color to -1, green to 0 and blue to 1
    # create a Rectangle patch
    ax[0].legend(handles=[Rectangle((0, 0), 1, 1, color='red'),
                          Rectangle((0, 0), 1, 1, color='green'),
                          Rectangle((0, 0), 1, 1, color='blue')],
                 labels=['Not enough data',
                         'Not Overlapping (<1%)', 'Overlapping (>=1%)'],
                 loc='upper right', fontsize=5)

    # iterare over all signal regions and add horizontal and vertical lines
    border_color = "k"
    for i in range(significant_correlations.shape[0]):
        ax[0].axhline(i - 0.5, color=border_color, linewidth=0.5)
        ax[0].axvline(i - 0.5, color=border_color, linewidth=0.5)
        ax[1].axhline(i - 0.5, color=border_color, linewidth=0.5)
        ax[1].axvline(i - 0.5, color=border_color, linewidth=0.5)

    # if mark_likelihood_scans is True, mark likelihood scans in the matrix (vertical and horizontal lines)
    if mark_likelihood_scans:
        likelihood_scans: Dict[str, Dict[str, List[Any]]] = __extract_likelihood_scan_indices(
            column_names=column_names)

        # mark analysis indices
        analyses: Dict[str, List[Any]] = __extract_analysis_indices(
            column_names=column_names)

        # loop over all likelihood scans
        for likelihood_scan in likelihood_scans:
            # get the index of the likelihood scan
            min_index = min(likelihood_scans[likelihood_scan]["index"])
            max_index = max(likelihood_scans[likelihood_scan]["index"])
            # add horizontal and vertical lines
            # for plot 1
            ax[0].axhline(min_index - 0.5, color=border_color, linewidth=2)
            ax[0].axhline(max_index + 0.5, color=border_color, linewidth=2)
            ax[0].axvline(min_index - 0.5, color=border_color, linewidth=2)
            ax[0].axvline(max_index + 0.5, color=border_color, linewidth=2)
            # for plot 2
            ax[1].axhline(min_index - 0.5, color=border_color, linewidth=2)
            ax[1].axhline(max_index + 0.5, color=border_color, linewidth=2)
            ax[1].axvline(min_index - 0.5, color=border_color, linewidth=2)
            ax[1].axvline(max_index + 0.5, color=border_color, linewidth=2)

        for analysis in analyses:
            # get the index of the analysis
            min_index = min(analyses[analysis])
            max_index = max(analyses[analysis])
            # add horizontal and vertical lines
            # for plot 1
            ax[0].axhline(min_index - 0.5, color=border_color, linewidth=4)
            ax[0].axhline(max_index + 0.5, color=border_color, linewidth=4)
            ax[0].axvline(min_index - 0.5, color=border_color, linewidth=4)
            ax[0].axvline(max_index + 0.5, color=border_color, linewidth=4)
            # for plot 2
            ax[1].axhline(min_index - 0.5, color=border_color, linewidth=4)
            ax[1].axhline(max_index + 0.5, color=border_color, linewidth=4)
            ax[1].axvline(min_index - 0.5, color=border_color, linewidth=4)
            ax[1].axvline(max_index + 0.5, color=border_color, linewidth=4)

    # if groupings are given, add horizontal and vertical lines (purple)
    if production_mode_grouping:
        # loop over all indices
        for production_index in production_indices:
            # add horizontal and vertical lines
            # for plot 1
            ax[0].axhline(production_index - 0.5,
                          color=border_color, linewidth=8)
            ax[0].axvline(production_index - 0.5,
                          color=border_color, linewidth=8)
            # for plot 2
            ax[1].axhline(production_index - 0.5,
                          color=border_color, linewidth=8)
            ax[1].axvline(production_index - 0.5,
                          color=border_color, linewidth=8)

    # if no_labels is True, remove labels
    if no_labels:
        ax[0].set_xticklabels([])
        ax[0].set_yticklabels([])
        ax[1].set_xticklabels([])
        ax[1].set_yticklabels([])

    # tight layout
    fig.tight_layout()

    # set figure titles
    ax[0].set_title("Significant correlations")
    ax[1].set_title("Upper limits")

    # save figure
    if save != "":
        plt.savefig(save)
        plt.close()
    else:
        plt.show()

    return


def plot_overlap_matrix_idx(
        file_index: int,
        result_dir: str,
        column_names: List[str] = [],
        correlation_method: str = "jaccard"):
    """
    Plot overlap matrix when overlap matrix evolution is enabled and saved and specific directory.

    Args:
        file_index (int): Index of overlap matrix file. "Point in time of overlap matrix".
        result_dir (str): Path to directory where overlap matrix files are saved.
        column_names(List[str]): List of column names. Defaults to [].
        correlation_method (str, optional): Method to calculate the correlation. Defaults to 'jaccard'.

    Returns:
        None
    """
    # check if directory exists
    if not os.path.isdir(result_dir):
        raise ValueError("Directory {} does not exist.".format(result_dir))

    # check if index exists for sum_x_y
    if not os.path.isfile(os.path.join(result_dir, f"sum_x_y_{file_index}.npy")):
        raise ValueError(
            f"File {os.path.join(result_dir, f'sum_x_y_{file_index}.npy')} does not exist.")

    # check if index exists for sum_x
    if not os.path.isfile(os.path.join(result_dir, f"sum_x_{file_index}.npy")):
        raise ValueError(
            f"File {os.path.join(result_dir, f'sum_x_{file_index}.npy')} does not exist.")

    # check if event file exists
    if not os.path.isfile(os.path.join(result_dir, f"n.txt")):
        raise ValueError(
            f"File {os.path.join(result_dir, f'n.txt')} does not exist.")

    # make a directory for images within the result_dir
    if not os.path.isdir(os.path.join(result_dir, "images")):
        os.mkdir(os.path.join(result_dir, "images"))

    # open event file and read number of events at file_index line
    with open(os.path.join(result_dir, f"n.txt"), "r") as f:
        lines = f.readlines()
        n = int(lines[file_index].split(":")[1].strip())

    # overlap matrix is not evaluated but the ingredients for it are saved
    sum_x_y: np.ndarray = np.load(os.path.join(
        result_dir, f"sum_x_y_{file_index}.npy"))
    sum_x: np.ndarray = np.load(os.path.join(
        result_dir, f"sum_x_{file_index}.npy"))

    overlap_matrix: np.ndarray = np.zeros(sum_x_y.shape).astype(float)

    if correlation_method == "geometrical" or correlation_method == "jaccard":
        overlap_matrix = jaccard_index(
            sum_x_y=sum_x_y,
            sum_x=sum_x)
    elif correlation_method == "szymkiewicz_simpson":
        overlap_matrix = szymkiewicz_simpson_index(
            sum_x_y=sum_x_y, sum_x=sum_x)
    else:
        raise NotImplementedError(
            "Correlation method {} not implemented.".format(correlation_method))

    # save plot
    plot_overlap_matrix(matrix=overlap_matrix,
                        column_names=column_names,
                        save=os.path.join(
                            result_dir,
                            "images",
                            f"overlap_matrix_{file_index}.png"),
                        events=n)

    return


def plot_statistics_matrices_idx(
        file_index: int,
        result_dir: str,
        column_names: List[str] = [],
        correlation_method: str = "jaccard",
        threshold: float = 0.01,
        confidence_level: float = 0.95):
    """
    Plot statistics matrices when overlap matrix evolution is enabled and saved and specific directory.

    Args:
        file_index (int): Index of overlap matrix file. "Point in time of overlap matrix".
        result_dir (str): Path to directory where overlap matrix files are saved.
        column_names(List[str]): List of column names. Defaults to [].
        correlation_method (str, optional): Method to calculate the correlation. Defaults to 'jaccard'.
        threshold (float, optional): Threshold for the probability of events to be accepted by both
            when accepting them by either one of them.
        confidence_level (float, optional): Significance level for the binomial test.
            Defaults to 0.95.

    Returns:
        None
    """
    # check if directory exists
    if not os.path.isdir(result_dir):
        raise ValueError("Directory {} does not exist.".format(result_dir))

    # check if index exists for sum_x_y
    if not os.path.isfile(os.path.join(result_dir, f"sum_x_y_{file_index}.npy")):
        raise ValueError(
            f"File {os.path.join(result_dir, f'sum_x_y_{file_index}.npy')} does not exist.")

    # check if index exists for sum_x
    if not os.path.isfile(os.path.join(result_dir, f"sum_x_{file_index}.npy")):
        raise ValueError(
            f"File {os.path.join(result_dir, f'sum_x_{file_index}.npy')} does not exist.")

    # check if event file exists
    if not os.path.isfile(os.path.join(result_dir, f"n.txt")):
        raise ValueError(
            f"File {os.path.join(result_dir, f'n.txt')} does not exist.")

    # make a directory for images within the result_dir
    if not os.path.isdir(os.path.join(result_dir, "images")):
        os.mkdir(os.path.join(result_dir, "images"))

    # open event file and read number of events at file_index line
    with open(os.path.join(result_dir, f"n.txt"), "r") as f:
        lines = f.readlines()
        n = int(lines[file_index].split(":")[1].strip())

    # statistics matrices are not evaluated but the ingredients for it are saved
    sum_x_y: np.ndarray = np.load(os.path.join(
        result_dir, f"sum_x_y_{file_index}.npy"))
    sum_x: np.ndarray = np.load(os.path.join(
        result_dir, f"sum_x_{file_index}.npy"))

    # shared events
    shared_events: np.ndarray = sum_x_y

    # accept events by either one of them
    accepted_events_either: np.ndarray = np.ones_like(
        sum_x_y) * sum_x + (np.ones_like(sum_x_y) * sum_x).T - sum_x_y

    # calculate statistics of correlations
    significant_correlations, upper_limits = calculate_statistics_of_correlations(
        shared_events=shared_events,
        accepted_events_either=accepted_events_either,
        threshold=threshold,
        confidence_level=confidence_level,
        correlation_method=correlation_method)

    # save plot
    plot_statistics_matrices(significant_correlations=significant_correlations,
                             upper_limits=upper_limits,
                             column_names=column_names,
                             save=os.path.join(
                                 result_dir, "images", f"statistics_{file_index}.png"),
                             events=n)

    return


def plot_events_matrices_idx(
        file_index: int,
        result_dir: str,
        column_names: List[str] = []):
    """
    Plot events matrices when overlap matrix evolution is enabled and saved and specific directory.

    Args:
        file_index (int): Index of overlap matrix file. "Point in time of overlap matrix".
        result_dir (str): Path to directory where overlap matrix files are saved.
        column_names(List[str]): List of column names. Defaults to [].

    Returns:
        None
    """
    # check if directory exists
    if not os.path.isdir(result_dir):
        raise ValueError("Directory {} does not exist.".format(result_dir))

    # check if index exists for sum_x_y
    if not os.path.isfile(os.path.join(result_dir, f"sum_x_y_{file_index}.npy")):
        raise ValueError(
            f"File {os.path.join(result_dir, f'sum_x_y_{file_index}.npy')} does not exist.")

    # check if index exists for sum_x
    if not os.path.isfile(os.path.join(result_dir, f"sum_x_{file_index}.npy")):
        raise ValueError(
            f"File {os.path.join(result_dir, f'sum_x_{file_index}.npy')} does not exist.")

    # check if event file exists
    if not os.path.isfile(os.path.join(result_dir, f"n.txt")):
        raise ValueError(
            f"File {os.path.join(result_dir, f'n.txt')} does not exist.")

    # make a directory for images within the result_dir
    if not os.path.isdir(os.path.join(result_dir, "images")):
        os.mkdir(os.path.join(result_dir, "images"))

    # open event file and read number of events at file_index line
    with open(os.path.join(result_dir, f"n.txt"), "r") as f:
        lines = f.readlines()
        n = int(lines[file_index].split(":")[1].strip())

    # statistics matrices are not evaluated but the ingredients for it are saved
    sum_x_y: np.ndarray = np.load(os.path.join(
        result_dir, f"sum_x_y_{file_index}.npy"))
    sum_x: np.ndarray = np.load(os.path.join(
        result_dir, f"sum_x_{file_index}.npy"))

    # shared events
    shared_events: np.ndarray = sum_x_y

    # accept events by either one of them
    accepted_events_either: np.ndarray = np.ones_like(
        sum_x_y) * sum_x + (np.ones_like(sum_x_y) * sum_x).T - sum_x_y

    # save plot
    plot_events_matrices(shared_events=shared_events,
                         accepted_events_either=accepted_events_either,
                         column_names=column_names,
                         save=os.path.join(
                             result_dir, "images", f"events_{file_index}.png"),
                         events=n)

    return


def plot_overlap_matrix_evolution_specific_entry(result_dir: str,
                                                 i: Union[int, List[int]],
                                                 j: Union[int, List[int]],
                                                 correlation_method: str = "jaccard"):
    """
    Plot overlap matrix evolution for specific entries i, j.

    Args:
        result_dir (str): Path to directory where overlap matrix files are saved.
        i (Union[int, List[int]]): Index of row.
        j (Union[int, List[int]]): Index of column.
        correlation_method (str, optional): Method to calculate the correlation. Defaults to 'jaccard'.

    Returns:
        None
    """
    # check the type of i and j, both should be the same and if list, same length
    if type(i) != type(j):
        raise ValueError("i and j should be of the same type.")
    if type(i) == list and len(i) != len(j):
        raise ValueError("i and j should have the same length.")

    # check if directory exists
    if not os.path.isdir(result_dir):
        raise ValueError(f"Directory {result_dir} does not exist.")

    # check if event file exists
    if not os.path.isfile(os.path.join(result_dir, f"n.txt")):
        raise ValueError(
            f"File {os.path.join(result_dir, f'n.txt')} does not exist.")

    # read from all sum_x_y and sum_x files the entries i, j
    sum_x_y_list: List[np.ndarray] = []
    sum_x_list: List[np.ndarray] = []

    # open event file and read number of events at file_index line
    with open(os.path.join(result_dir, f"n.txt"), "r") as f:
        lines = f.readlines()
        indices: List[int] = [int(line.split(":")[0].strip())
                              for line in lines]
        n: List[int] = [int(line.split(":")[1].strip()) for line in lines]

    for counter, idx in enumerate(indices):
        # statistics matrices are not evaluated but the ingredients for it are saved
        sum_x_y: np.ndarray = np.load(os.path.join(
            result_dir, f"sum_x_y_{idx}.npy"))[i, j]
        sum_x: np.ndarray = np.load(os.path.join(
            result_dir, f"sum_x_{idx}.npy"))
        print(f"{counter}/{len(indices)}")

        sum_x_y_list.append(sum_x_y)
        sum_x_list.append(sum_x)

    # make the lists numpy arrays
    sum_x_y_list: np.ndarray = np.array(sum_x_y_list)
    sum_x_list: np.ndarray = np.array(sum_x_list)

    if correlation_method == "geometrical" or correlation_method == "jaccard":
        overlap = sum_x_y_list / \
            (sum_x_list[:, i] + sum_x_list[:, j] - sum_x_y_list)
    elif correlation_method == "szymkiewicz_simpson":
        # TODO: Check if this is true
        overlap = sum_x_y_list / np.minimum(sum_x_list[:, i], sum_x_list[:, j])

    # set image size
    plt.figure(figsize=(10, 10))

    # plot overlap matrix indices evolution
    if type(i) == list:
        for k in range(len(i)):
            plt.plot(indices, overlap[:, k], label=f"{i[k]}, {j[k]}")
    else:
        plt.plot(indices, overlap[:], "k.", label=f"{i}, {j}")

    plt.xlabel("File Index")
    plt.ylabel("Overlap Matrix Entry")

    # scale log
    plt.yscale("log")

    # legend
    plt.legend()

    # tight layout
    plt.tight_layout()

    # save plot
    plt.savefig(os.path.join(result_dir, "images",
                f"overlap_matrix_evolution_specific_entry.png"))

    # close plot
    plt.close()

    return


def plot_matrix_differences(matrix_A: np.ndarray,
                            matrix_B: np.ndarray,
                            column_names: List[str] = [],
                            no_labels: bool = False,
                            mark_likelihood_scans: bool = False,
                            production_mode_grouping: Dict[str, str] = {},
                            save: str = "") -> None:
    """
    Plot matrix differences.

    Args:
        matrix_A (np.ndarray): Matrix A.
        matrix_B (np.ndarray): Matrix B.
        column_names(List[str]): List of column names. Defaults to [].
        no_labels (bool): Do not show labels. Defaults to False.
        mark_likelihood_scans (bool): Mark likelihood scans in the matrix (vertical and horizontal lines).
        production_mode_grouping (Dict[str, str]): Dictionary with production mode grouping. Defaults to {}.
        save (str): Path to save plot.

    Returns:
        None
    """
    # when production mode grouping is given, group the columns (1. EW, 2. Strong, 3. Other)
    if production_mode_grouping != {}:
        EW_column_names: List[str] = []
        Strong_column_names: List[str] = []
        Other_column_names: List[str] = []
        # itertae over all column names
        for column_name in column_names:
            analysis_name = column_name.split("__")[0].strip()
            if production_mode_grouping[analysis_name] == "EW":
                EW_column_names.append(column_name)
            elif production_mode_grouping[analysis_name] == "Strong":
                Strong_column_names.append(column_name)
            else:
                Other_column_names.append(column_name)

        # sort all column names alphabetically
        EW_column_names.sort()
        Strong_column_names.sort()
        Other_column_names.sort()

        # assign indices
        production_indices: List[int] = [len(EW_column_names),
                                         len(EW_column_names) + len(Strong_column_names)]

        # concatenate all column names
        column_names_new = EW_column_names + Strong_column_names + Other_column_names

        # sort the matrix
        matrix_A = sort_matrix_column_names(matrix=matrix_A,
                                            column_names_original=column_names,
                                            column_names_new=column_names_new)
        matrix_B = sort_matrix_column_names(matrix=matrix_B,
                                            column_names_original=column_names,
                                            column_names_new=column_names_new)

        # set column names
        column_names = column_names_new
    # check if matrices have the same shape
    if matrix_A.shape != matrix_B.shape:
        raise ValueError(
            "Matrices have different shapes: {} and {}".format(matrix_A.shape, matrix_B.shape))

    # define figure 1,2 subplots
    fig, ax = plt.subplots(1, 2, figsize=(20, 10))

    # set ATLAS style
    set_plot_style(matrix=True)

    # the first subplot shows the absolute difference between matrix A and B
    cbar = ax[0].figure.colorbar(ax[0].imshow(np.abs(matrix_A - matrix_B).round(5),
                                              cmap='Greys', norm=LogNorm(vmin=1e-7, vmax=1)), ax=ax[0])
    cbar.ax.tick_params(labelsize=10)
    cbar.ax.set_ylabel("Absolute Difference", fontdict={
        "family": "sans-serif", "fontsize": 18, })

    # the second subplot shows in which matrix entry the difference is != 0
    ax[1].imshow(np.isclose(
        np.abs(matrix_A - matrix_B).round(5), 0), cmap='Greys')

    changed_entries = np.sum(np.isclose(
        np.abs(matrix_A - matrix_B).round(5), 1))
    unchanged_entries = np.sum(np.isclose(
        np.abs(matrix_A - matrix_B).round(5), 1) == False)

    # add legend to second plot
    ax[1].legend(handles=[Rectangle((0, 0), 1, 1, facecolor='white'),
                          Rectangle((0, 0), 1, 1, facecolor='black')],
                 labels=[f"Changed Entries: {changed_entries}",
                         f"Unchanged Entries: {unchanged_entries}"],
                 loc='upper right', fontsize=5)

    # Make labels centered
    ax[0].xaxis.set_ticks_position("top")
    ax[0].xaxis.set_label_position("top")
    ax[1].xaxis.set_ticks_position("top")
    ax[1].xaxis.set_label_position("top")
    # Set the labels
    ax[0].set_xticks(np.arange(len(column_names)), minor=False)
    ax[0].set_yticks(np.arange(len(column_names)), minor=False)
    ax[0].set_xticklabels(column_names, fontfamily="sans-serif")
    ax[0].set_yticklabels(column_names, fontfamily="sans-serif")
    ax[1].set_xticks(np.arange(len(column_names)), minor=False)
    ax[1].set_yticks(np.arange(len(column_names)), minor=False)
    ax[1].set_xticklabels(column_names, fontfamily="sans-serif")
    ax[1].set_yticklabels(column_names, fontfamily="sans-serif")
    # Rotate the x labels
    plt.setp(ax[0].get_xticklabels(), rotation=90,
             ha="left", rotation_mode="anchor")
    plt.setp(ax[1].get_xticklabels(), rotation=90,
             ha="left", rotation_mode="anchor")

    # iterare over all signal regions and add horizontal and vertical lines
    border_color = "k"
    for i in range(matrix_A.shape[0]):
        ax[0].axhline(i - 0.5, color=border_color, linewidth=0.5)
        ax[0].axvline(i - 0.5, color=border_color, linewidth=0.5)
        ax[1].axhline(i - 0.5, color=border_color, linewidth=0.5)
        ax[1].axvline(i - 0.5, color=border_color, linewidth=0.5)

    # if mark_likelihood_scans is True, mark likelihood scans in the matrix (vertical and horizontal lines)
    if mark_likelihood_scans:
        likelihood_scans: Dict[str, Dict[str, List[Any]]] = __extract_likelihood_scan_indices(
            column_names=column_names)

        # mark analysis indices
        analyses: Dict[str, List[Any]] = __extract_analysis_indices(
            column_names=column_names)

        # loop over all likelihood scans
        for likelihood_scan in likelihood_scans:
            # get the index of the likelihood scan
            min_index = min(likelihood_scans[likelihood_scan]["index"])
            max_index = max(likelihood_scans[likelihood_scan]["index"])
            # add horizontal and vertical lines
            # for plot 1
            ax[0].axhline(min_index - 0.5, color=border_color, linewidth=2)
            ax[0].axhline(max_index + 0.5, color=border_color, linewidth=2)
            ax[0].axvline(min_index - 0.5, color=border_color, linewidth=2)
            ax[0].axvline(max_index + 0.5, color=border_color, linewidth=2)
            # for plot 2
            ax[1].axhline(min_index - 0.5, color=border_color, linewidth=2)
            ax[1].axhline(max_index + 0.5, color=border_color, linewidth=2)
            ax[1].axvline(min_index - 0.5, color=border_color, linewidth=2)
            ax[1].axvline(max_index + 0.5, color=border_color, linewidth=2)

        for analysis in analyses:
            # get the index of the analysis
            min_index = min(analyses[analysis])
            max_index = max(analyses[analysis])
            # add horizontal and vertical lines
            # for plot 1
            ax[0].axhline(min_index - 0.5, color=border_color, linewidth=4)
            ax[0].axhline(max_index + 0.5, color=border_color, linewidth=4)
            ax[0].axvline(min_index - 0.5, color=border_color, linewidth=4)
            ax[0].axvline(max_index + 0.5, color=border_color, linewidth=4)
            # for plot 2
            ax[1].axhline(min_index - 0.5, color=border_color, linewidth=4)
            ax[1].axhline(max_index + 0.5, color=border_color, linewidth=4)
            ax[1].axvline(min_index - 0.5, color=border_color, linewidth=4)
            ax[1].axvline(max_index + 0.5, color=border_color, linewidth=4)

    # if groupings are given, add horizontal and vertical lines (purple)
    if production_mode_grouping:
        # loop over all indices
        for production_index in production_indices:
            # add horizontal and vertical lines
            # for plot 1
            ax[0].axhline(production_index - 0.5,
                          color=border_color, linewidth=8)
            ax[0].axvline(production_index - 0.5,
                          color=border_color, linewidth=8)
            # for plot 2
            ax[1].axhline(production_index - 0.5,
                          color=border_color, linewidth=8)
            ax[1].axvline(production_index - 0.5,
                          color=border_color, linewidth=8)

    # if no_labels is True, remove labels
    if no_labels:
        ax[0].set_xticklabels([])
        ax[0].set_yticklabels([])
        ax[1].set_xticklabels([])
        ax[1].set_yticklabels([])

    # set figure titles
    ax[0].set_title("Absolute Difference")
    ax[1].set_title("Difference")

    # tight layout
    fig.tight_layout()

    # save figure
    if save != "":
        plt.savefig(save)
        plt.close()
    else:
        plt.show()

    return


def plot_UL_OVER_scatter(upper_limits_matrix: np.ndarray,
                         overlaps_matrix: np.ndarray,
                         save: str = "") -> None:
    """
    Method for plotting upper limits vs overlap matrix.

    Args:
        upper_limits_matrix (np.ndarray): Upper limits matrix.
        overlap_matrix (np.ndarray): Overlap matrix.
        save (str): Path to save plot.

    Returns:
        None
    """
    # Check that the shapes of the matrices are the same
    if upper_limits_matrix.shape != overlaps_matrix.shape:
        raise ValueError(
            "Matrices have different shapes: {} and {}".format(upper_limits_matrix.shape, overlaps_matrix.shape))

    # set image size
    fig, ax = plt.subplots(1, 1, figsize=(10, 10))

    # set ATLAS plot style
    set_plot_style(matrix=False)

    # flatten matrices
    upper_limits_matrix = upper_limits_matrix.flatten()
    overlaps_matrix = overlaps_matrix.flatten()

    # plot scatter
    ax.scatter(overlaps_matrix, upper_limits_matrix, s=1)

    # set labels
    ax.set_xlabel("Overlap", fontsize=14, fontfamily="sans-serif")
    ax.set_ylabel("Upper limits", fontsize=14, fontfamily="sans-serif")

    # set log scale on the upper limits axis
    ax.set_yscale("log")

    # tight layout
    fig.tight_layout()

    # save figure
    if save != "":
        plt.savefig(save)
        plt.close()
    else:
        plt.show()

    return


if __name__ == "__main__":
    df = pd.read_csv(os.path.join(os.path.dirname(__file__),
                     "..", "processing_tools", "final_setup.csv"))
    plot_transition_matrix(df=df,
                           save="transition_matrix",
                           xvar="SATACO_Comparison_BestExpCLs_simplified",
                           yvar="EW_ExpCLs_SATACO_Analysis")
