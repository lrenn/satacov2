"""
Plot distributions of different things.
"""

import math
from typing import Any, Dict, List

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from SATACO.learning_tools.actively_learn_overlaps import (
    all_pMSSM_parameters, restrict_parameter_length)
from SATACO.plotting_tools.plot_style import COLORS, set_plot_style


def plot_overlap_distribution(overlap_matrix: np.ndarray,
                              upper_limit: np.ndarray = None,
                              save: str = None,
                              log_scale: bool = False):
    """
    Returns a plot of the overlap distribution (only upper triangle) and 
    the upper limit (if given).

    Args:
        overlap_matrix (np.ndarray): Overlap matrix that holds values
            for distribution.
        upper_limit (np.ndarray): Upper limit for the overlap. Default is None.
        save (str): Path to save the plot. Default is None.
        log_scale (bool): If True, the bins are logarithmically spaced.
            Default is False.

    Returns:
        None
    """
    fig, ax = plt.subplots(figsize=(10, 10))

    # set ATLAS plot style
    set_plot_style(matrix=False)

    if log_scale:
        bins = np.logspace(-6, 0, 30)
        # add to the bins also 0
        bins = np.insert(bins, 0, 0)
    else:
        bins = np.linspace(0, 1, 30)

    # plot histogram (from the overlap matrix just the upper triangle is taken
    # with the diagonal)
    ax.hist(overlap_matrix[np.triu_indices(
        overlap_matrix.shape[0], k=1)],
        bins=bins,
        color=COLORS[1],
        alpha=0.5)

    # if upper limit is given, plot it (do not fill and give it a black edge,
    # color not None)
    if upper_limit is not None:
        ax.hist(upper_limit[np.triu_indices(
            upper_limit.shape[0], k=1)],
            bins=bins,
            fill=False,
            edgecolor='k')

    # set x-axis to log scale
    ax.set_xlabel('Overlap Value')

    # set y-axis to log scale
    ax.set_ylabel('Number of combinations')

    # set the maximum of the y-axis to the maximum of the histogram
    ax.set_ylim(1, np.max(ax.get_yticks()))

    # set x-axis to log scale
    if log_scale:
        ax.set_xscale('log')

    # set y-axis to log scale
    ax.set_yscale('log')

    # add legend
    if upper_limit is not None:
        ax.legend(['Overlap',
                   'Upper limit'])

    # tight layout
    plt.tight_layout()

    # save plot if path is given
    if save is not None:
        plt.savefig(save)
    else:
        plt.show()

    return


def plot_allowed_combinations_threshold(overlap_matrix: np.ndarray,
                                        upper_limit: np.ndarray = None,
                                        save: str = None,
                                        current_threshold: float = None):
    """
    Plot the number of allowed combinations for different thresholds.

    Args:
        overlap_matrix (np.ndarray): Overlap matrix that holds values
            for distribution.
        upper_limit (np.ndarray): Upper limit for the overlap. Default is None.
        save (str): Path to save the plot. Default is None.
        current_threshold (float): Current threshold. Default is None.

    Returns:
        None
    """
    # thresholds to test
    thresholds: np.ndarray = np.logspace(-6, 0, 50)

    # legend entries
    legend_entries: List[str] = ['Overlap']

    # number of allowed combinations for different thresholds
    allowed_combinations_overlap: List[int] = []
    if upper_limit is not None:
        allowed_combinations_upper_limit: List[int] = []
        legend_entries.append('Upper limit')

    # loop over thresholds
    for threshold in thresholds:
        # get indices of allowed combinations
        allowed_combinations_overlap.append(
            np.sum(overlap_matrix[np.triu_indices(
                overlap_matrix.shape[0], k=1)] <= threshold))
        if upper_limit is not None:
            allowed_combinations_upper_limit.append(
                np.sum(upper_limit[np.triu_indices(
                    upper_limit.shape[0], k=1)] <= threshold))

    # plot number of allowed combinations for different thresholds
    fig, ax = plt.subplots(figsize=(10, 10))

    # set ATLAS plot style
    set_plot_style(matrix=False)

    ax.plot(thresholds, allowed_combinations_overlap, color='b')

    if upper_limit is not None:
        ax.plot(thresholds, allowed_combinations_upper_limit, color='k')

    if current_threshold is not None:
        ax.axvline(current_threshold, color='r')
        legend_entries.append('Current threshold')

    # plot a horizontal line that shows the number of combinations
    ax.axhline(allowed_combinations_overlap[-1], color='k', linestyle='--')
    legend_entries.append('Total number of combinations')

    # set x-axis to log scale
    ax.set_xscale('log')

    # set y-axis to log scale
    ax.set_yscale('log')

    # set x-axis label
    ax.set_xlabel('Threshold')

    # set y-axis label
    ax.set_ylabel('Allowed combinations')

    # add legend
    ax.legend(legend_entries)

    # tight layout
    plt.tight_layout()

    for idx, allowed_combs in enumerate(allowed_combinations_overlap):
        if allowed_combs != 0:
            if idx != 0:
                ax.set_xlim(thresholds[idx - 1], thresholds[-1])
                break

    # save plot if path is given
    if save is not None:
        plt.savefig(save)
    else:
        plt.show()

    return


def plot_multiple_indices_distributions(
    overlap_values_list: List[List[float]],
    index_names_list: List[List[str]] = None,
    save: str = None
) -> None:
    """
    Plot the distributions of multiple indices with a 3x3 subplot layout. The
    last overlap value in each list is taken as the combined index.

    Args:
        overlap_values_list (List[List[float]]): List of lists of overlap
            values, with each list representing a set of distributions.
        index_names_list (List[List[str]]): List of lists of index names.
            Default is None.
        save (str): Path to save the plot. Default is None.

    Returns:
        None
    """
    if len(overlap_values_list) > 9:
        raise ValueError("Cannot plot more than 9 distributions at once.")

    num_subplots = len(overlap_values_list)

    # Determine the number of rows and columns for subplots
    if num_subplots <= 3:
        num_rows, num_cols = 1, num_subplots
    elif num_subplots <= 6:
        num_rows, num_cols = 2, num_subplots // 2
    else:
        num_rows, num_cols = 3, 3

    # Create subplots
    fig, axs = plt.subplots(num_rows,
                            num_cols,
                            figsize=(7 * num_cols,
                                     7 * num_rows),
                            sharey=True,
                            sharex=True)

    # Flatten the axs array for indexing convenience
    axs = np.ravel(axs)

    # Set up a color cycle for plotting
    color_cycle = plt.cm.viridis(np.linspace(0, 1, num_subplots))

    ylimits = []

    for i, (overlap_values, ax) in enumerate(zip(overlap_values_list, axs)):
        # plot histogram for each set of distributions
        hist, _, _ = ax.hist(
            overlap_values,
            bins=30,
            color=color_cycle[i],
            alpha=0.3,
            label=f'Overlap Values from {len(overlap_values) - 1} Models')

        # mark with a vertical line the last overlap value
        ax.axvline(overlap_values[-1],
                   color="k",
                   linestyle='--',
                   label='Combined Matrix Value')

        # set labels
        ax.set_ylabel('Number of Combinations')

        ylimits.append(np.max(hist))

        # set y-axis to log scale
        ax.set_yscale('log')

        # set x-axis label
        ax.set_xlabel('Overlap Value')

        # add legend
        ax.legend()

        # add title
        if index_names_list is not None and i < len(index_names_list):
            title = '\n'.join(index_names_list[i])
            ax.set_title(f'Distribution of \n{title}')
        else:
            ax.set_title(f'Distribution {i + 1}')

    # Set the ylimits to be the same for all subplots
    for ax in axs:
        ax.set_ylim(1, np.max(ylimits) + 1)

    # Tight layout
    plt.tight_layout()

    # Save plot if path is given
    if save is not None:
        plt.savefig(save)
    else:
        plt.show()

    return


def plot_low_acceptance_distributions(
        efficiencies_df: pd.DataFrame,
        parameters_df: pd.DataFrame,
        efficiencies_threshold: float = 0.0,
        save: str = None) -> None:
    """
    Function that plots the distribution of pMSSM parameters that have low
    eifficiencies across (almost all) SRs.

    efficiencies_df:
    ----------------------------------------------------------------
    | Model | <Analysis>__<SRi> | ... | <Analysis>__<SRk> |    n   |
    ----------------------------------------------------------------
    |  1    | 0                 | ... | 20                | 100000 |
    ...
    ----------------------------------------------------------------

    parameters_df:
    -----------------------------
    | Model | M_1  | ... | tanb |
    -----------------------------
    |  1    | 20.0 | ... | -1    |
    ...
    -----------------------------

    Args:
        efficiencies_df (pd.DataFrame): DataFrame with the efficiencies
            for each SR.
        parameters_df (pd.DataFrame): DataFrame with the parameters for
            each model.
        efficiencies_threshold (float): Threshold for the efficiencies.
            Default is 0.0.
        save (str): Path to save the plot. Default is None.

    Returns:
        None
    """
    # In the efficiencies df divide all columns besides of the 'Model'
    # and 'n' column by the 'n' column which is the number of events
    # use apply to divide all columns by the 'n' column
    columns_to_divide = [column for column in efficiencies_df.columns
                         if column not in ['Model', 'n']]
    efficiencies_df[columns_to_divide] = efficiencies_df[columns_to_divide].apply(
        lambda x: x / efficiencies_df['n'])

    # Only keep the models that efficiencies below the threshold
    efficiencies_df[columns_to_divide] = efficiencies_df[columns_to_divide][
        efficiencies_df[columns_to_divide] <= efficiencies_threshold]

    # Drop nan values
    efficiencies_df = efficiencies_df.dropna()

    # Drop the 'n' column
    efficiencies_df = efficiencies_df.drop(columns=['n'])

    # Get the models that have low efficiencies across all SRs
    low_efficiencies_models = efficiencies_df['Model'].values

    print(f'There are {len(low_efficiencies_models)} models with efficiencies '
          f'below {efficiencies_threshold}.')

    # Keep only the parameters of the models with low efficiencies
    low_efficiencies_parameters = parameters_df.loc[
        parameters_df['Model'].isin(low_efficiencies_models)]

    # Restrict the length of the parameters
    pMSSM_parameters: List[str] = restrict_parameter_length(
        inferrence_parameters=all_pMSSM_parameters())

    low_efficiencies_parameters = low_efficiencies_parameters[pMSSM_parameters]

    # The number of parameters to plot is 19
    # subplot layout 4x5
    num_rows, num_cols = 4, 5

    # Create subplots
    fig, axs = plt.subplots(num_rows,
                            num_cols,
                            figsize=(7 * num_cols,
                                     7 * num_rows))

    set_plot_style(matrix=False)

    # plot histogram for each parameter
    for i, (parameter, ax) in enumerate(
            zip(low_efficiencies_parameters.columns, axs.ravel())):
        ax.hist(low_efficiencies_parameters[parameter],
                bins=10,
                color='b',
                alpha=0.3)

        # set labels
        ax.set_ylabel('Number of odels')

        # set y-axis to log scale
        # ax.set_yscale('log')

        # set x-axis label
        ax.set_xlabel(parameter)

        # add title
        ax.set_title(f'Distribution of {parameter}')

    # Tight layout
    plt.tight_layout()

    # Save plot if path is given
    if save is not None:
        plt.savefig(save)
    else:
        plt.show()

    return


def bar_plot_acccepted_events(
        accepted_events: Dict[str, int],
        focus_analysis: str = None,
        save: str = None) -> None:
    """
    Plot the number of accepted events for each SR in the analysis
    as a horizontal bar plot.

    Args:
        accepted_events (Dict[str, int]): Dictionary with the number of
            accepted events for each SR.
        focus_analysis (str): Name of the analysis to focus on.
            Default is None.
        save (str): Path to save the plot. Default is None.

    Returns:
        None
    """
    # get the analysis names
    analysis_names: List[str] = [
        key.split('__')[0] for key in accepted_events.keys()]

    # names of signal regions
    signal_region_names: List[str] = []

    # make a set to get unique analysis names
    analysis_names = sorted(list(set(analysis_names)))

    # check if focus analysis is given
    if focus_analysis is not None:
        # check if focus analysis is in the dictionary
        if focus_analysis not in analysis_names:
            raise KeyError(f'Analysis {focus_analysis} not in dictionary.')

        # iterate over the keys of the dictionary that are in the focus analysis
        for key in accepted_events.keys():
            if key.split('__')[0] == focus_analysis:
                signal_region_names.append(key)

        # define a figure
        fig, ax = plt.subplots(figsize=(10, 10))

        # set ATLAS plot style

        # plot the number of accepted events for each SR
        ax.barh(signal_region_names,
                [accepted_events[key] for key in signal_region_names],
                color='b',
                alpha=0.3)

        # set x-axis label
        ax.set_xlabel('Number of Accepted Events')

        # add title
        ax.set_title(focus_analysis)

    else:

        # make for each analysis a subplot (two analysis per row)
        fig, axs = plt.subplots(math.ceil(len(analysis_names) / 2),
                                2,
                                figsize=(10,
                                         math.ceil(len(analysis_names) / 2) * 5))

        # flatten the axs array for indexing convenience
        axs = np.ravel(axs)

        # plot the number of accepted events for each SR
        for i, (analysis_name, ax) in enumerate(zip(analysis_names, axs)):
            # iterate over the keys of the dictionary that are in the analysis
            for key in accepted_events.keys():
                if key.split('__')[0] == analysis_name:
                    signal_region_names.append(key)

            # plot the number of accepted events for each SR
            ax.barh(signal_region_names,
                    [accepted_events[key] for key in signal_region_names],
                    color='b',
                    alpha=0.3)

            # set x-axis label
            ax.set_xlabel('Number of Accepted Events')

            # add title
            ax.set_title(analysis_name)

            # reset signal region names
            signal_region_names = []

    # Tight layout
    plt.tight_layout()

    # Save plot if path is given
    if save is not None:
        plt.savefig(save)
    else:
        plt.show()

    return


def plot_path_distributions(
        paths: List[str],
        path_lengths: List[int],
        log_scale: bool = False,
        save: str = None) -> None:
    """
    The paths are in the format <SRi>&<SRj>&...&<SRk> and the path lengths
    are the number of SRs in the path.

    Args:
        paths (List[str]): List of paths.
        path_lengths (List[int]): List of path lengths.
        save (str): Path to save the plot. Default is None.

    Returns:
        None
    """

    # distribution of path lengths
    fig, ax = plt.subplots(figsize=(10, 10))

    # set ATLAS plot style
    set_plot_style(matrix=False)

    # plot histogram of path lengths
    ax.hist(path_lengths,
            bins=30,
            color='b',
            alpha=0.3)

    # set y-axis to log scale
    if log_scale:
        ax.set_yscale('log')

    # set x-axis label
    ax.set_xlabel('Length')

    # set y-axis label
    ax.set_ylabel('Number of paths')

    # tight layout
    plt.tight_layout()

    # save plot if path is given
    if save is not None:
        plt.savefig(save)
    else:
        plt.show()

    return
