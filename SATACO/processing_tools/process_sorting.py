"""
Resort a matrix.
"""

import numpy as np
from typing import List, Tuple, Union

def sort_matrix_column_names(matrix: np.ndarray,
                             column_names_original: List[str],
                             column_names_new: List[str]) -> np.ndarray:
    """
    Sort the matrix according to the new column names.

    Args:
        matrix (np.ndarray): Matrix to be sorted.
        column_names_original (List[str]): Original column names.
        column_names_new (List[str]): New column names.

    Returns:
        np.ndarray: Sorted matrix.
    """
    # Check that the column names are unique.
    if len(column_names_new) != len(set(column_names_new)):
        raise ValueError("The column names are not unique.")
    
    # Check that the column names are the same.
    if set(column_names_original) != set(column_names_new):
        raise ValueError("The column names are not the same.")
    
    # Check that the matrix has the same number of columns as the column names.
    if matrix.shape[1] != len(column_names_original):
        raise ValueError("The matrix has a different number of columns than the column names.")
    
    # Get the indices of the new column names.
    indices = [column_names_original.index(column_name) for column_name in column_names_new]

    # Sort the matrix.
    matrix_sorted = matrix[:, indices]
    matrix_sorted = matrix_sorted[indices, :]

    return matrix_sorted

