# SATACO


## Description
The package contains all tools for combining different searches in SUSY analyses and will be dominantly used within the [pMSSMFactory](https://gitlab.cern.ch/atlas-phys-susy-wg/summaries/pmssm/pMSSMFactory).

## Installlation

 To use the package, you need to install it first. The package is based on the gitlab of cern and can therefore be installed by using the following command:

```bash
pip install git+https://gitlab.cern.ch/lrenn/sataco
```

If you want to work on the package, you can clone it first and afterwards work on it locally:

```bash
git clone https://gitlab.cern.ch/lrenn/sataco
cd sataco
```

The tools are written specifically for the pMSSMFactory and therefore in python **3.7.6**. To make sure that you are using the correct python version, you can create a virtual environment and install all necessary packages in it. To create a virtual environment, you can use the following command:

```bash
cd ..
python -m venv sataco-venv
source sataco-venv/bin/activate
```

After activating this virtual environment, you can install all necessary packages:

```bash
cd sataco
pip install -r requirements.txt
```

Now you are ready to work on the package.

## Usage

As stated above the code is written for a specific framework and therefore the usage is limited to this framework. However, If you find code that is useful for you, feel free to use and modify it. (Don't forget to give credit to the authors of the code.)

## Packages used in SATACO

matplotlib<br>
numpy<br>
pandas<br>
networkx<br>
scipy<br>
pytest<br>
more-itertools<br>
scikit-optimize<br>



